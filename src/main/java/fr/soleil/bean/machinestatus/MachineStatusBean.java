package fr.soleil.bean.machinestatus;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.LineBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.bean.machinestatus.components.BeamLineLabel;
import fr.soleil.bean.machinestatus.components.MachineStatusChart;
import fr.soleil.bean.machinestatus.customtargets.NumberMatrixTargetAdapter;
import fr.soleil.bean.machinestatus.customtargets.TextMatrixTargetAdapter;
import fr.soleil.bean.machinestatus.model.FrontEndState;
import fr.soleil.bean.machinestatus.model.FunctionMode;
import fr.soleil.bean.machinestatus.model.IColorConstant;
import fr.soleil.bean.machinestatus.model.IHtmlConstant;
import fr.soleil.bean.machinestatus.model.IMachineStatusConstants;
import fr.soleil.bean.machinestatus.model.IModelConnector;
import fr.soleil.bean.machinestatus.model.MachineStatusModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IAutoScrolledComponent.ScrollMode;
import fr.soleil.comete.swing.AutoScrolledTextField;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.event.GroupEvent;
import fr.soleil.data.listener.IRefreshingGroupListener;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.GroupRefreshingStrategy;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;
import fr.soleil.lib.project.swing.text.DynamicForegroundLabel;

/**
 * This is the SOLEIL Comete bean for the MachineStatus device.
 * <bt />
 * It allows to take screenshots.
 * <bt />
 * Messages fields are writable in operator mode.
 * 
 * @author Loic VIMONT, Sandra PIERRE-JOSEPH, Raphael GIRARDOT
 */
public class MachineStatusBean extends AbstractTangoBox
        implements IMachineStatusConstants, IColorConstant, IHtmlConstant {

    private static final long serialVersionUID = -6097616047553033251L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MachineStatusBean.class);

    private static final ImageIcon FILL_COLORS_HELP_ICON = new ImageIcon(
            MachineStatusBean.class.getResource("image/chart_fill_colors_help.png"));

    private static final int DEFAULT_SCREENSHOT_PERIOD = 30;
    private static final boolean DEFAULT_SCREENSHOT_ENABLED = false;
    private static final String DEFAULT_SCREENSHOT_FILE_NAME = "machinestatus";
    private static final String DEFAULT_SCREENSHOT_EXTENSION = "png";
    private static final String DEFAULT_SCREENSHOT_DIRECTORY = ".//MachineStatus";
    private static final String SMIS = "SMIS";
    private static final String AILES = "AILES";

    // cf JIRA CONTROLGUI-69 et CONTROLGUI-38
    private static final boolean DEFAULT_IS_CHART_SAMPLED = false;
    private static final int DEFAULT_MESSAGE_GROUP_REFRESH = 1500;
    private static final int DEFAULT_TREND_GROUP_REFRESH = 5550;
    private static final int DEFAULT_BEAM_INFO_GROUP_REFRESH = 960;
    private static final int DEFAULT_BEAMLINES_GROUP_REFRESH = 1260;
    private static final int DEFAULT_CURRENT_GROUP_REFRESH = 810;

    // Interface mode constants
    public static final String OPERATOR_MODE = "operator";
    public static final String USER_MODE = "user";

    private static final String TEXT_FONT_NAME = "Arial";
    private static final String VALUE_FONT_NAME = "SansSerif";
    private static final int EXT_MARGIN = 30;
    private static final int HGAP = 1;
    private static final int VGAP = 1;
    private final static Color LIGHTBLUE_BACKGROUND = new Color(0, 150, 200);
    private final static Color BLUE_BACKGROUND = Color.BLUE;
    private static final Font DATE_FONT = new Font(TEXT_FONT_NAME, Font.PLAIN, 36);
    private static final Font TITLE_FONT = new Font(TEXT_FONT_NAME, Font.PLAIN, 26);
    private static final Font CURRENT_FONT = new Font(TEXT_FONT_NAME, Font.BOLD, 46);
    private static final Font USER_INFO_FONT = new Font(TEXT_FONT_NAME, Font.PLAIN, 22);
    private static final Font INFORMATION_FONT = new Font(TEXT_FONT_NAME, Font.PLAIN, 36);
    private static final Font INFORMATION2_FONT = new Font(TEXT_FONT_NAME, Font.PLAIN, 30);
    private static final Font VALUE_FONT = new Font(VALUE_FONT_NAME, Font.BOLD, 32);
    private static final Font VALUE_FONT2 = new Font(VALUE_FONT_NAME, Font.BOLD, 28);
    private static final Font RING_TITLE_FONT = new Font(TEXT_FONT_NAME, Font.PLAIN, 20);
    private static final Font RING_VALUE_FONT = new Font(VALUE_FONT_NAME, Font.BOLD, 22);
    private static final Font BEAM_TITLE_FONT = new Font(VALUE_FONT_NAME, Font.BOLD, 22);
    private static final Font TDL_FONT = new Font(VALUE_FONT_NAME, Font.PLAIN, 22);
    private static final ImageIcon LOGO = new ImageIcon(
            MachineStatusBean.class.getResource("/fr/soleil/bean/machinestatus/image/soleilquadri.png"));

    // attribute groups
    private static final Map<String, String> GROUP_MAP;

    // State colors
    private static final Map<Integer, Color> STATE_COLORS;

    static {
        // creation of an unmodifiable color map
        Map<Integer, Color> colorMap = new HashMap<>();
        for (FrontEndState state : FrontEndState.values()) {
            colorMap.put(state.getValue(), state.getColor());
        }
        STATE_COLORS = Collections.unmodifiableMap(colorMap);

        Map<String, String> groupMap = new HashMap<String, String>() {
            private static final long serialVersionUID = -8388684700903559128L;

            @Override
            public String put(String key, String value) {
                return super.put(key == null ? null : key.toLowerCase(), value);
            }
        };
        // current
        groupMap.put(CURRENT, CURRENT_GROUP);
        groupMap.put(FUNCTION_MODE, CURRENT_GROUP);
        groupMap.put(FILLING_MODE, CURRENT_GROUP);
        groupMap.put(LIFETIME, CURRENT_GROUP);
        groupMap.put(CURRENT_TOTAL, CURRENT_GROUP);
        groupMap.put(AVERAGE_PRESSURE, CURRENT_GROUP);
        // beamlines
        groupMap.put(NAME_OF_BENDINGS, BEAMLINES_GROUP);
        groupMap.put(STATE_OF_BENDINGS, BEAMLINES_GROUP);
        groupMap.put(NAME_OF_IDS, BEAMLINES_GROUP);
        groupMap.put(STATE_OF_IDS, BEAMLINES_GROUP);
        groupMap.put(FRONT_END_STATE_COLOR, BEAMLINES_GROUP);
        // beamInfo
        groupMap.put(USABLE_SINCE, BEAM_INFO_GROUP);
        groupMap.put(NEXT_USER_BEAM, BEAM_INFO_GROUP);
        groupMap.put(END_OF_CURRENT_FUNCTION_MODE, BEAM_INFO_GROUP);
        groupMap.put(USER_INFO, BEAM_INFO_GROUP);
        groupMap.put(H_RMS_ORBIT, BEAM_INFO_GROUP);
        groupMap.put(V_RMS_ORBIT, BEAM_INFO_GROUP);
        groupMap.put(H_EMITTANCE, BEAM_INFO_GROUP);
        groupMap.put(V_EMITTANCE, BEAM_INFO_GROUP);
        groupMap.put(H_TUNES, BEAM_INFO_GROUP);
        groupMap.put(V_TUNES, BEAM_INFO_GROUP);
        // message
        groupMap.put(MESSAGE, MESSAGE_GROUP);
        groupMap.put(OPERATOR_MESSAGE, MESSAGE_GROUP);
        groupMap.put(OPERATOR_MESSAGE2, MESSAGE_GROUP);
        // trend
        groupMap.put(CURRENT_TREND, TREND_GROUP);
        groupMap.put(CURRENT_TREND_TIMES, TREND_GROUP);
        groupMap.put(FUNCTION_MODE_TREND, TREND_GROUP);
        groupMap.put(MAX_TREND_VALUE, TREND_GROUP);
        groupMap.put(TICK_SPACING, TREND_GROUP);
        groupMap.put(DEFAULT_MODES_COLOR, TREND_GROUP);
        GROUP_MAP = Collections.unmodifiableMap(groupMap);
    }

    private final Map<String, Integer> periodMap;

    private final Map<Integer, Color> frontEndStateColors;
    private String interfaceMode;
    // refreshing times from properties
    private int currentGroupPeriod;
    private int beamlinesGroupPeriod;
    private int beamInfoGroupPeriod;
    private int trendGroupPeriod;

    public int getTrendGroupPeriod() {
        return trendGroupPeriod;
    }

    private int messageGroupPeriod;
    // asynchronous group refreshing for better polling

    private boolean chartSampled;
    // Screenshooting
    private String directory;
    private String imageExtension;
    private String fileName;
    private int period;
    private boolean isScreenshotEnabled;

    private JPanel northPanel, centerPanel, southPanel;
    private final IModelConnector model;
    private final MachineStatusChart chart;
    private JPanel insertionBeamPanel, bendingBeamPanel, infraredPanel;
    private JLabel dateLabel, timeLabel, functionModeLabel, fillingModeLabel, lifetimeLabel, currentTotalLabel,
            averagePressurelabel, deliveryLabel, remainingTimeLabel, hLabel, vLabel, orbitRMSLabel, emittanceLabel,
            tuneLabel, insertionLabel, bendingLabel, infraredLabel, logoLabel;
    private Label currentViewer, functionModeViewer, fillingModeViewer, lifetimeViewer, currentTotalViewer,
            averagePressureViewer, deliveryViewer, userInfoViewer, endOfBeamViewer, remainingTimeViewer, hOrbitViewer,
            vOrbitViewer, hEmittanceViewer, vEmittanceViewer, hTuneViewer, vTuneViewer, operatorMessageDateViewer;
    private AutoScrolledTextField messageViewer, operatorMessageValueViewer, operatorMessage2Viewer;
    private String messageDate, messageValue;
    private volatile String[] beamLinesTab, beamLinesTab2;
    private volatile boolean beamLinesTabChanged, beamLinesTab2Changed;
    private volatile BeamLineLabel[] beamLinesLabels, beamLinesLabels2;
    private volatile int[] stateCodeID, stateCodeBM;
    private volatile boolean stateCodeIDChanged, stateCodeBMChanged;
    private final StringScalarBox stringScalarBox;
    private final StringMatrixBox stringMBox;
    private final StringMatrixBox stringColorMBox;
    private final NumberMatrixBox numberMBox;
    private final BeamLinesGroupListener beamLinesGroupListener;
    private IKey keyCurrent, keyFunctionMode, keyFrontEndStateColor, keyNameOfIDs, keyStateOfIDs, keyNameOfBendings,
            keyStateOfBendings, keyFillingMode, keyLifetime, keyCurrentTotal, keyAveragePressure, keyDelivery,
            keyEndOfBeam, keyRemainingTime, keyUserInfo, keyHOrbit, keyVOrbit, keyHEmittance, keyVEmittance, keyHTune,
            keyVTune, keyMessage, keyOperatorMessage, keyOperatorMessage2;
    private GridBagConstraints northPanelConstraints, centerPanelConstraints, chartConstraints, southPanelConstraints;
    private StringSpliter stringSpliter;
    private FrontEndStateReader frontEndStateReader;
    private StringTabReader stringTabReader;
    private String2TabReader string2TabReader;
    private NumberTabReader numberTabReader;
    private Number2TabReader number2TabReader;
    private Timer timer;
    private Timer dateTimer;
    protected File screenFile;
    private DeviceProxy devProxy;
    private DeviceAttribute devAttr;
    private String[] frontColorsMemoryTab;
    private volatile boolean isColorFromAttr;
    private volatile boolean disconnecting;
    private final JPopupMenu feStateColorsPopupMenu;
    private final JMenu chartColorsMenu;

    // ---------------------------------------------------------------------------------------------------

    public MachineStatusBean() {
        super();
        feStateColorsPopupMenu = new JPopupMenu();
        feStateColorsPopupMenu.setBackground(STANDARD_GRAY_BACKGROUND);
        JPanel stateColorsPanel = new JPanel(new GridBagLayout());
        stateColorsPanel.setOpaque(false);
        int y = 0;
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = y++;
        constraints.weightx = 1;
        constraints.weighty = 0;
        stateColorsPanel.add(new JLabel("Front End state colors:", JLabel.CENTER), constraints);
        for (FrontEndState state : FrontEndState.values()) {
            constraints.gridy = y++;
            stateColorsPanel.add(new JSeparator(), constraints);
            DynamicForegroundLabel label = new DynamicForegroundLabel(state.getName());
            label.setAntiAliasingEnabled(true);
            label.setBackground(state.getColor());
            label.setOpaque(true);
            constraints.gridy = y++;
            stateColorsPanel.add(label, constraints);
        }
        feStateColorsPopupMenu.add(stateColorsPanel);
        chartColorsMenu = new JMenu("Fill colors help");
        chartColorsMenu.setIcon(FILL_COLORS_HELP_ICON);
        JPanel chartColorsPanel = new JPanel(new GridBagLayout());
        chartColorsPanel.setOpaque(false);
        y = 0;
        constraints.gridy = y++;
        chartColorsPanel.add(new JLabel("Fill colors (function modes):", FILL_COLORS_HELP_ICON, JLabel.CENTER),
                constraints);
        for (FunctionMode mode : FunctionMode.values()) {
            if (mode != FunctionMode.UNKNOWN_MODE) {
                constraints.gridy = y++;
                chartColorsPanel.add(new JSeparator(), constraints);
                StringBuilder builder = new StringBuilder(HTML_TITLE_START);
                builder.append(mode.getName()).append(HTML_DESCRIPTION_SEPARATOR);
                builder.append(mode.getDescription()).append(HTML_DESCRIPTION_END);
                DynamicForegroundLabel label = new DynamicForegroundLabel(builder.toString());
                label.setAntiAliasingEnabled(true);
                label.setBackground(mode.getColor());
                label.setOpaque(true);
                constraints.gridy = y++;
                chartColorsPanel.add(label, constraints);
            }
        }
        chartColorsMenu.add(chartColorsPanel);
        frontEndStateColors = new ConcurrentHashMap<>();

        timer = null;
        dateTimer = null;
        frontColorsMemoryTab = null;
        isColorFromAttr = false;
        disconnecting = false;

        loadBootProperties();
        Map<String, Integer> tmpMap = new HashMap<>();
        tmpMap.put(CURRENT_GROUP, currentGroupPeriod);
        tmpMap.put(BEAMLINES_GROUP, beamlinesGroupPeriod);
        tmpMap.put(BEAM_INFO_GROUP, beamInfoGroupPeriod);
        tmpMap.put(TREND_GROUP, trendGroupPeriod);
        tmpMap.put(MESSAGE_GROUP, messageGroupPeriod);
        periodMap = Collections.unmodifiableMap(tmpMap);

        stringScalarBox = new StringScalarBox();
        stringMBox = new StringMatrixBox();
        stringColorMBox = new StringMatrixBox();
        numberMBox = new NumberMatrixBox();
        beamLinesGroupListener = new BeamLinesGroupListener();
        chart = new MachineStatusChart();
        chart.getChartView().getComponentPopupMenu().add(chartColorsMenu);
        model = new MachineStatusModel(this);
        setPanels();
    }

    public boolean isChartSampled() {
        return chartSampled;
    }

    private void loadBootProperties() {
        File file = new File("machinestatus.properties");
        if (file.exists()) {
            InputStream strm = null;
            try {
                strm = new FileInputStream(file);
                System.getProperties().load(strm);
                LOGGER.info("MachineStatus configured from {}", file.getCanonicalPath());
            } catch (Exception e) {
                LOGGER.error("Error setting initialisation properties", e);
            } finally {
                if (strm != null) {
                    try {
                        strm.close();
                    } catch (Exception e) {
                    }
                }
            }
        } else {
            LOGGER.info("No properties file : -D or default value are used");
        }

        interfaceMode = System.getProperty("machinestatus.interface.mode", USER_MODE);
        isScreenshotEnabled = Boolean.valueOf(
                System.getProperty("machinestatus.screenshot.enable", Boolean.toString(DEFAULT_SCREENSHOT_ENABLED)))
                .booleanValue();

        // read refreshing periods
        currentGroupPeriod = Integer.parseInt(System.getProperty("machinestatus.period.currentgroup",
                Integer.toString(DEFAULT_CURRENT_GROUP_REFRESH)));
        beamlinesGroupPeriod = Integer.parseInt(System.getProperty("machinestatus.period.beamlinesgroup",
                Integer.toString(DEFAULT_BEAMLINES_GROUP_REFRESH)));
        beamInfoGroupPeriod = Integer.parseInt(System.getProperty("machinestatus.period.beaminfogroup",
                Integer.toString(DEFAULT_BEAM_INFO_GROUP_REFRESH)));
        trendGroupPeriod = Integer.parseInt(
                System.getProperty("machinestatus.period.trendgroup", Integer.toString(DEFAULT_TREND_GROUP_REFRESH)));
        messageGroupPeriod = Integer.parseInt(System.getProperty("machinestatus.period.messagegroup",
                Integer.toString(DEFAULT_MESSAGE_GROUP_REFRESH)));
        // enable chart sampling
        chartSampled = Boolean.parseBoolean(
                System.getProperty("machinestatus.chart.sampling", Boolean.toString(DEFAULT_IS_CHART_SAMPLED)));
        if (isScreenshotEnabled) {
            directory = System.getProperty("machinestatus.screenshot.directory", DEFAULT_SCREENSHOT_DIRECTORY);
            imageExtension = System.getProperty("machinestatus.screenshot.extension", DEFAULT_SCREENSHOT_EXTENSION);
            fileName = System.getProperty("machinestatus.screenshot.filename", DEFAULT_SCREENSHOT_FILE_NAME);
            period = Integer.parseInt(
                    System.getProperty("machinestatus.screenshot.period", Integer.toString(DEFAULT_SCREENSHOT_PERIOD)));
        }

        StringBuilder builder = new StringBuilder();
        builder.append("\"the value of SCREENSHOT_ENABLE is \"").append(isScreenshotEnabled).append('\n');
        builder.append("IsChartSampled = ").append(chartSampled).append('\n');
        builder.append("currentGroupPeriod = ").append(currentGroupPeriod).append('\n');
        builder.append("beamlinesGroupPeriod = ").append(beamlinesGroupPeriod).append('\n');
        builder.append("beamInfoGroupPeriod = ").append(beamInfoGroupPeriod).append('\n');
        builder.append("trendGroupPeriod = ").append(trendGroupPeriod).append('\n');
        builder.append("messageGroupPeriod = ").append(messageGroupPeriod);
        LOGGER.debug(builder.toString());
    }

    public boolean isDisconnecting() {
        return disconnecting;
    }

    public void setDisconnecting(boolean disconnecting) {
        this.disconnecting = disconnecting;
    }

    private void setGeneralGridBagLayout() {
        this.setLayout(new GridBagLayout());
        northPanelConstraints = new GridBagConstraints();
        northPanelConstraints.gridy = 0;
        northPanelConstraints.weightx = 1;
        northPanelConstraints.weighty = 0;
        northPanelConstraints.insets = new Insets(EXT_MARGIN, EXT_MARGIN, 0, EXT_MARGIN);
        northPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        centerPanelConstraints = new GridBagConstraints();
        centerPanelConstraints.gridy = 1;
        centerPanelConstraints.weightx = 1;
        centerPanelConstraints.weighty = 0;
        centerPanelConstraints.insets = new Insets(10, EXT_MARGIN, 10, EXT_MARGIN);
        centerPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        chartConstraints = new GridBagConstraints();
        chartConstraints.gridy = 2;
        chartConstraints.weightx = 1;
        chartConstraints.weighty = 1;
        chartConstraints.insets = new Insets(0, EXT_MARGIN, 0, EXT_MARGIN);
        chartConstraints.fill = GridBagConstraints.BOTH;
        southPanelConstraints = new GridBagConstraints();
        southPanelConstraints.gridy = 3;
        southPanelConstraints.weightx = 1;
        southPanelConstraints.weighty = 0;
        southPanelConstraints.insets = new Insets(0, EXT_MARGIN, EXT_MARGIN, EXT_MARGIN);
        southPanelConstraints.fill = GridBagConstraints.HORIZONTAL;

    }

    private void setPanels() {
        setGeneralGridBagLayout();
        this.setBackground(STANDARD_GRAY_BACKGROUND);

        stringSpliter = new StringSpliter();
        frontEndStateReader = new FrontEndStateReader();
        stringTabReader = new StringTabReader();
        string2TabReader = new String2TabReader();
        numberTabReader = new NumberTabReader();
        number2TabReader = new Number2TabReader();

        dateLabel = new JLabel(ObjectUtils.EMPTY_STRING);
        dateLabel.setFont(DATE_FONT);
        dateLabel.setHorizontalAlignment(SwingConstants.CENTER);
        timeLabel = new JLabel(ObjectUtils.EMPTY_STRING);
        timeLabel.setFont(DATE_FONT);
        timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        functionModeLabel = new JLabel("Function Mode");
        functionModeLabel.setFont(TITLE_FONT);
        functionModeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        fillingModeLabel = new JLabel("Filling Mode");
        fillingModeLabel.setFont(TITLE_FONT);
        fillingModeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lifetimeLabel = new JLabel("Lifetime");
        lifetimeLabel.setFont(TITLE_FONT);
        lifetimeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        currentTotalLabel = new JLabel("Integrated Current");
        currentTotalLabel.setFont(TITLE_FONT);
        currentTotalLabel.setHorizontalAlignment(SwingConstants.CENTER);
        averagePressurelabel = new JLabel("Average Pressure");
        averagePressurelabel.setFont(TITLE_FONT);
        averagePressurelabel.setHorizontalAlignment(SwingConstants.CENTER);
        insertionLabel = new JLabel("Insertion Devices");
        insertionLabel.setFont(BEAM_TITLE_FONT);
        insertionLabel.setHorizontalAlignment(SwingConstants.CENTER);
        insertionLabel.setComponentPopupMenu(feStateColorsPopupMenu);
        bendingLabel = new JLabel("Bending Magnet");
        bendingLabel.setFont(BEAM_TITLE_FONT);
        bendingLabel.setHorizontalAlignment(SwingConstants.CENTER);
        bendingLabel.setComponentPopupMenu(feStateColorsPopupMenu);
        infraredLabel = new JLabel("Infrared ");
        infraredLabel.setFont(BEAM_TITLE_FONT);
        infraredLabel.setHorizontalAlignment(SwingConstants.CENTER);
        deliveryLabel = new JLabel("Delivery Since");
        deliveryLabel.setFont(USER_INFO_FONT);
        deliveryLabel.setHorizontalAlignment(SwingConstants.CENTER);
        remainingTimeLabel = new JLabel("Remaining Time");
        remainingTimeLabel.setFont(USER_INFO_FONT);
        remainingTimeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        hLabel = new JLabel("H");
        hLabel.setFont(RING_TITLE_FONT);
        hLabel.setHorizontalAlignment(SwingConstants.CENTER);
        vLabel = new JLabel("V");
        vLabel.setFont(RING_TITLE_FONT);
        vLabel.setHorizontalAlignment(SwingConstants.CENTER);
        orbitRMSLabel = new JLabel("Orbit(RMS)");
        orbitRMSLabel.setFont(RING_TITLE_FONT);
        orbitRMSLabel.setHorizontalAlignment(SwingConstants.CENTER);
        emittanceLabel = new JLabel("Emittance");
        emittanceLabel.setFont(RING_TITLE_FONT);
        emittanceLabel.setHorizontalAlignment(SwingConstants.CENTER);
        tuneLabel = new JLabel("Tune");
        tuneLabel.setFont(RING_TITLE_FONT);
        tuneLabel.setHorizontalAlignment(SwingConstants.CENTER);

        currentViewer = generateBlueLabel();
        currentViewer.setFont(CURRENT_FONT);
        functionModeViewer = generateBlueLabel();
        functionModeViewer.setFont(VALUE_FONT);
        fillingModeViewer = generateBlueLabel();
        fillingModeViewer.setFont(VALUE_FONT);
        lifetimeViewer = generateBlueLabel();
        lifetimeViewer.setFont(VALUE_FONT);
        currentTotalViewer = generateBlueLabel();
        currentTotalViewer.setFont(VALUE_FONT);
        averagePressureViewer = generateLightBlueLabel();
        averagePressureViewer.setFont(VALUE_FONT2);
        deliveryViewer = generateBlueLabel();
        deliveryViewer.setForeground(Color.WHITE);
        deliveryViewer.setFont(VALUE_FONT2);
        userInfoViewer = generateBeanLabel();
        userInfoViewer.setOpaque(false);
        userInfoViewer.setFont(USER_INFO_FONT);
        userInfoViewer.setBorder(new LineBorder(Color.black, 2));
        endOfBeamViewer = generateBlueLabel();
        endOfBeamViewer.setFont(VALUE_FONT2);
        remainingTimeViewer = generateBlueLabel();
        remainingTimeViewer.setFont(VALUE_FONT2);
        hOrbitViewer = generateLightBlueLabel();
        hOrbitViewer.setFont(RING_VALUE_FONT);
        vOrbitViewer = generateLightBlueLabel();
        vOrbitViewer.setFont(RING_VALUE_FONT);
        hEmittanceViewer = generateLightBlueLabel();
        hEmittanceViewer.setFont(RING_VALUE_FONT);
        vEmittanceViewer = generateLightBlueLabel();
        vEmittanceViewer.setFont(RING_VALUE_FONT);
        hTuneViewer = generateLightBlueLabel();
        hTuneViewer.setFont(RING_VALUE_FONT);
        vTuneViewer = generateLightBlueLabel();
        vTuneViewer.setFont(RING_VALUE_FONT);

        // editable viewers if INTERFACE_MODE == OPERATOR_MODE
        messageViewer = new AutoScrolledTextField();
        messageViewer.setBorder(null);
        messageViewer.setFont(VALUE_FONT);
        messageViewer.setBackground(BLUE_BACKGROUND);
        messageViewer.setForeground(Color.WHITE);
        messageViewer.setHorizontalAlignment(SwingConstants.CENTER);
        messageViewer.setFocusable(false);
        operatorMessageDateViewer = generateLightBlueLabel();
        operatorMessageDateViewer.setFont(RING_VALUE_FONT);
        operatorMessageValueViewer = new AutoScrolledTextField();
        operatorMessageValueViewer.setBorder(null);
        operatorMessageValueViewer.setFont(INFORMATION_FONT);
        operatorMessageValueViewer.setBackground(LIGHTBLUE_BACKGROUND);
        operatorMessageValueViewer.setForeground(Color.WHITE);
        operatorMessageValueViewer.setHorizontalAlignment(SwingConstants.CENTER);
        operatorMessageValueViewer.setAutoscrolls(true);
        operatorMessageValueViewer.setFocusable(false);
        operatorMessageValueViewer.setScrollMode(ScrollMode.PENDULAR);
        operatorMessage2Viewer = new AutoScrolledTextField();
        operatorMessage2Viewer.setBorder(null);
        operatorMessage2Viewer.setFont(INFORMATION2_FONT);
        operatorMessage2Viewer.setBackground(LIGHTBLUE_BACKGROUND);
        operatorMessage2Viewer.setForeground(Color.WHITE);
        operatorMessage2Viewer.setHorizontalAlignment(SwingConstants.CENTER);
        operatorMessage2Viewer.setAutoscrolls(true);
        operatorMessage2Viewer.setScrollMode(ScrollMode.LOOP);
        operatorMessage2Viewer.setFocusable(false);

        if (OPERATOR_MODE.equals(interfaceMode)) { // if in operator mode --> editable with input dialogs
            LOGGER.debug("Operator mode activated");
            messageViewer.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent arg0) {
                    String message = (String) JOptionPane.showInputDialog(MachineStatusBean.this,
                            "Enter a new run message", "Change run message", JOptionPane.QUESTION_MESSAGE, null, null,
                            messageViewer.getText());
                    if (message != null) {
                        try {
                            devProxy = new DeviceProxy(getModel());
                            devAttr = new DeviceAttribute(MESSAGE, message);
                            devProxy.write_attribute(devAttr);
                        } catch (DevFailed e) {
                            LOGGER.error("Device connection error while writing new run message : {}", e);
                        }
                    }
                }
            });
            operatorMessageValueViewer.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent arg0) {
                    String message = (String) JOptionPane.showInputDialog(MachineStatusBean.this,
                            "Enter a new operator message", "Change operator message", JOptionPane.QUESTION_MESSAGE,
                            null, null, operatorMessageValueViewer.getText());
                    if (message != null) {
                        try {
                            devProxy = new DeviceProxy(getModel());
                            devAttr = new DeviceAttribute(OPERATOR_MESSAGE, message);
                            devProxy.write_attribute(devAttr);
                        } catch (DevFailed e) {
                            LOGGER.error("Device connection error while writing new operator message : {} ", e);
                        }
                    }
                }
            });
            operatorMessage2Viewer.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent arg0) {
                    String message = (String) JOptionPane.showInputDialog(MachineStatusBean.this,
                            "Enter a new information message", "Change information message",
                            JOptionPane.QUESTION_MESSAGE, null, null, operatorMessage2Viewer.getText());
                    if (message != null) {
                        try {
                            devProxy = new DeviceProxy(getModel());
                            devAttr = new DeviceAttribute(OPERATOR_MESSAGE2, message);
                            devProxy.write_attribute(devAttr);
                        } catch (DevFailed e) {
                            LOGGER.error("Device connection error while writing new operator message 2 : {}", e);
                        }
                    }
                }
            });
        }

        this.add(getNorthPanel(), northPanelConstraints);
        this.add(getCenterPanel(), centerPanelConstraints);
        this.add(chart, chartConstraints);
        this.add(getSouthPanel(), southPanelConstraints);
    }

    private Label generateBeanLabel() {
        Label label = new Label();
        label.setAntiAliasingEnabled(true);
        label.setOpaque(true);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        return label;
    }

    private Label generateBlueLabel() {
        Label label = generateBeanLabel();
        label.setBackground(BLUE_BACKGROUND);
        label.setForeground(Color.WHITE);
        return label;
    }

    private Label generateLightBlueLabel() {
        Label label = generateBeanLabel();
        label.setBackground(LIGHTBLUE_BACKGROUND);
        label.setForeground(Color.WHITE);
        return label;
    }

    private JPanel getNorthPanel() {
        northPanel = new JPanel(new GridBagLayout());

        GridBagConstraints currentPanelConstraints = new GridBagConstraints();
        currentPanelConstraints.fill = GridBagConstraints.BOTH;
        currentPanelConstraints.weightx = 1;
        currentPanelConstraints.weighty = 0;
        currentPanelConstraints.insets = new Insets(0, 0, 0, 0);
        currentPanelConstraints.gridx = 0;

        GridBagConstraints beamLinesConstraints = new GridBagConstraints();
        beamLinesConstraints.fill = GridBagConstraints.BOTH;
        beamLinesConstraints.weightx = 1;
        beamLinesConstraints.weighty = 0;
        beamLinesConstraints.insets = new Insets(0, 20, 0, 0);
        beamLinesConstraints.gridx = 1;

        northPanel.add(getCurrentPanel(), currentPanelConstraints);
        northPanel.add(getBeamLinesPanel(), beamLinesConstraints);
        northPanel.setBackground(STANDARD_GRAY_BACKGROUND);
        return northPanel;
    }

    private JPanel getCurrentPanel() {
        JPanel currentPanel = new JPanel(new GridBagLayout());

        GridBagConstraints dateConstraints = new GridBagConstraints();
        dateConstraints.fill = GridBagConstraints.BOTH;
        dateConstraints.weightx = 0;
        dateConstraints.weighty = 0;
        dateConstraints.insets = new Insets(0, 0, 0, 10);
        dateConstraints.gridx = 0;
        dateConstraints.gridy = 0;

        GridBagConstraints timeConstraints = new GridBagConstraints();
        timeConstraints.fill = GridBagConstraints.BOTH;
        timeConstraints.weightx = 0;
        timeConstraints.weighty = 0;
        timeConstraints.insets = new Insets(0, 0, 0, 10);
        timeConstraints.gridx = 0;
        timeConstraints.gridy = 1;

        GridBagConstraints functionModeLabelConstraints = new GridBagConstraints();
        functionModeLabelConstraints.fill = GridBagConstraints.BOTH;
        functionModeLabelConstraints.weightx = 0;
        functionModeLabelConstraints.weighty = 0;
        functionModeLabelConstraints.insets = new Insets(0, 0, 0, 10);
        functionModeLabelConstraints.gridx = 0;
        functionModeLabelConstraints.gridy = 2;

        GridBagConstraints fillingModeLabelConstraints = new GridBagConstraints();
        fillingModeLabelConstraints.fill = GridBagConstraints.BOTH;
        fillingModeLabelConstraints.weightx = 0;
        fillingModeLabelConstraints.weighty = 0;
        fillingModeLabelConstraints.insets = new Insets(0, 0, 0, 10);
        fillingModeLabelConstraints.gridx = 0;
        fillingModeLabelConstraints.gridy = 3;

        GridBagConstraints lifetimeLabelConstraints = new GridBagConstraints();
        lifetimeLabelConstraints.fill = GridBagConstraints.BOTH;
        lifetimeLabelConstraints.weightx = 0;
        lifetimeLabelConstraints.weighty = 0;
        lifetimeLabelConstraints.insets = new Insets(0, 0, 0, 10);
        lifetimeLabelConstraints.gridx = 0;
        lifetimeLabelConstraints.gridy = 4;

        GridBagConstraints integratedCurrentLabelConstraints = new GridBagConstraints();
        integratedCurrentLabelConstraints.fill = GridBagConstraints.BOTH;
        integratedCurrentLabelConstraints.weightx = 0;
        integratedCurrentLabelConstraints.weighty = 0;
        integratedCurrentLabelConstraints.insets = new Insets(0, 0, 0, 10);
        integratedCurrentLabelConstraints.gridx = 0;
        integratedCurrentLabelConstraints.gridy = 5;

        GridBagConstraints averagePressurelabelConstraints = new GridBagConstraints();
        averagePressurelabelConstraints.fill = GridBagConstraints.BOTH;
        averagePressurelabelConstraints.weightx = 0;
        averagePressurelabelConstraints.weighty = 0;
        averagePressurelabelConstraints.insets = new Insets(0, 0, 0, 10);
        averagePressurelabelConstraints.gridx = 0;
        averagePressurelabelConstraints.gridy = 6;

        GridBagConstraints currentConstraints = new GridBagConstraints();
        currentConstraints.fill = GridBagConstraints.BOTH;
        currentConstraints.weightx = 1;
        currentConstraints.weighty = 0;
        currentConstraints.insets = new Insets(0, 0, 8, 0);
        currentConstraints.gridx = 1;
        currentConstraints.gridy = 0;
        currentConstraints.gridheight = 2;

        GridBagConstraints functionModeConstraints = new GridBagConstraints();
        functionModeConstraints.fill = GridBagConstraints.BOTH;
        functionModeConstraints.weightx = 1;
        functionModeConstraints.weighty = 0;
        functionModeConstraints.insets = new Insets(0, 0, 8, 0);
        functionModeConstraints.gridx = 1;
        functionModeConstraints.gridy = 2;

        GridBagConstraints fillingModeConstraints = new GridBagConstraints();
        fillingModeConstraints.fill = GridBagConstraints.BOTH;
        fillingModeConstraints.weightx = 1;
        fillingModeConstraints.weighty = 0;
        fillingModeConstraints.insets = new Insets(0, 0, 8, 0);
        fillingModeConstraints.gridx = 1;
        fillingModeConstraints.gridy = 3;

        GridBagConstraints lifetimeConstraints = new GridBagConstraints();
        lifetimeConstraints.fill = GridBagConstraints.BOTH;
        lifetimeConstraints.weightx = 1;
        lifetimeConstraints.weighty = 0;
        lifetimeConstraints.insets = new Insets(0, 0, 8, 0);
        lifetimeConstraints.gridx = 1;
        lifetimeConstraints.gridy = 4;

        GridBagConstraints currentTotalConstraints = new GridBagConstraints();
        currentTotalConstraints.fill = GridBagConstraints.BOTH;
        currentTotalConstraints.weightx = 1;
        currentTotalConstraints.weighty = 0;
        currentTotalConstraints.insets = new Insets(0, 0, 8, 0);
        currentTotalConstraints.gridx = 1;
        currentTotalConstraints.gridy = 5;

        GridBagConstraints averagePressureConstraints = new GridBagConstraints();
        averagePressureConstraints.fill = GridBagConstraints.BOTH;
        averagePressureConstraints.weightx = 1;
        averagePressureConstraints.weighty = 0;
        averagePressureConstraints.insets = new Insets(0, 0, 0, 0);
        averagePressureConstraints.gridx = 1;
        averagePressureConstraints.gridy = 6;

        currentPanel.add(dateLabel, dateConstraints);
        currentPanel.add(timeLabel, timeConstraints);
        currentPanel.add(functionModeLabel, functionModeLabelConstraints);
        currentPanel.add(fillingModeLabel, fillingModeLabelConstraints);
        currentPanel.add(lifetimeLabel, lifetimeLabelConstraints);
        currentPanel.add(currentTotalLabel, integratedCurrentLabelConstraints);
        currentPanel.add(averagePressurelabel, averagePressurelabelConstraints);
        currentPanel.add(currentViewer, currentConstraints);
        currentPanel.add(functionModeViewer, functionModeConstraints);
        currentPanel.add(fillingModeViewer, fillingModeConstraints);
        currentPanel.add(lifetimeViewer, lifetimeConstraints);
        currentPanel.add(currentTotalViewer, currentTotalConstraints);
        currentPanel.add(averagePressureViewer, averagePressureConstraints);

        currentPanel.setBackground(STANDARD_GRAY_BACKGROUND);
        return currentPanel;
    }

    private JPanel getBeamLinesPanel() {
        JPanel beamLinesPanel = new JPanel(new GridBagLayout());
        insertionBeamPanel = new JPanel();
        insertionBeamPanel.setComponentPopupMenu(feStateColorsPopupMenu);
        bendingBeamPanel = new JPanel();
        bendingBeamPanel.setComponentPopupMenu(feStateColorsPopupMenu);
        infraredPanel = new JPanel();
        insertionBeamPanel.setBackground(STANDARD_GRAY_BACKGROUND);
        bendingBeamPanel.setBackground(STANDARD_GRAY_BACKGROUND);
        infraredPanel.setBackground(STANDARD_GRAY_BACKGROUND);

        GridBagConstraints insertionLabelConstraints = new GridBagConstraints();
        insertionLabelConstraints.fill = GridBagConstraints.BOTH;
        insertionLabelConstraints.weightx = 1;
        insertionLabelConstraints.weighty = 0;
        insertionLabelConstraints.insets = new Insets(0, 0, 0, 0);
        insertionLabelConstraints.gridx = 1;
        insertionLabelConstraints.gridy = 0;
        GridBagConstraints bendingLabelConstraints = new GridBagConstraints();
        bendingLabelConstraints.fill = GridBagConstraints.BOTH;
        bendingLabelConstraints.weightx = 0;
        bendingLabelConstraints.weighty = 0;
        bendingLabelConstraints.insets = new Insets(0, 0, 0, 10);
        bendingLabelConstraints.gridx = 0;
        bendingLabelConstraints.gridy = 0;
        GridBagConstraints insertionConstraints = new GridBagConstraints();
        insertionConstraints.fill = GridBagConstraints.BOTH;
        insertionConstraints.weightx = 0;
        insertionConstraints.weighty = 1;
        insertionConstraints.insets = new Insets(0, 0, 0, 0);
        insertionConstraints.gridx = 1;
        insertionConstraints.gridy = 1;
        GridBagConstraints bendingConstraints = new GridBagConstraints();
        bendingConstraints.fill = GridBagConstraints.BOTH;
        bendingConstraints.weightx = 0;
        bendingConstraints.weighty = 1;
        bendingConstraints.insets = new Insets(0, 0, 0, 10);
        bendingConstraints.gridx = 0;
        bendingConstraints.gridy = 1;
        GridBagConstraints infraredConstraints = new GridBagConstraints();
        infraredConstraints.fill = GridBagConstraints.HORIZONTAL;
        infraredConstraints.weightx = 1;
        infraredConstraints.weighty = 0;
        infraredConstraints.insets = new Insets(10, 0, 0, 0);
        infraredConstraints.gridx = 1;
        infraredConstraints.gridy = 2;

        beamLinesPanel.add(insertionLabel, insertionLabelConstraints);
        beamLinesPanel.add(bendingLabel, bendingLabelConstraints);
        beamLinesPanel.add(infraredPanel, infraredConstraints);
        beamLinesPanel.add(insertionBeamPanel, insertionConstraints);
        beamLinesPanel.add(bendingBeamPanel, bendingConstraints);

        beamLinesPanel.setBackground(STANDARD_GRAY_BACKGROUND);

        return beamLinesPanel;
    }

    private JPanel getCenterPanel() {
        centerPanel = new JPanel(new GridBagLayout());
        JPanel boxPanel = new JPanel();
        boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.Y_AXIS));
        boxPanel.setBackground(STANDARD_GRAY_BACKGROUND);
        Dimension dimension = new Dimension(270, 40);
        deliveryLabel.setMaximumSize(dimension);
        deliveryLabel.setMinimumSize(dimension);
        deliveryLabel.setPreferredSize(dimension);
        userInfoViewer.setMaximumSize(dimension);
        userInfoViewer.setMinimumSize(dimension);
        userInfoViewer.setPreferredSize(dimension);
        remainingTimeLabel.setMaximumSize(dimension);
        remainingTimeLabel.setMinimumSize(dimension);
        remainingTimeLabel.setPreferredSize(dimension);
        boxPanel.add(deliveryLabel);
        boxPanel.add(userInfoViewer);
        boxPanel.add(remainingTimeLabel);

        GridBagConstraints logoConstraints = new GridBagConstraints();
        logoConstraints.fill = GridBagConstraints.BOTH;
        logoConstraints.weightx = 0;
        logoConstraints.weighty = 1;
        logoConstraints.insets = new Insets(0, 0, 0, 10);
        logoConstraints.gridx = 0;
        logoConstraints.gridy = 0;
        logoConstraints.gridheight = 4;

        GridBagConstraints boxConstraints = new GridBagConstraints();
        boxConstraints.fill = GridBagConstraints.BOTH;
        boxConstraints.weightx = 0;
        boxConstraints.weighty = 1;
        boxConstraints.insets = new Insets(0, 0, 0, 0);
        boxConstraints.gridx = 1;
        boxConstraints.gridy = 1;
        boxConstraints.gridheight = 3;

        GridBagConstraints deliveryViewerConstraints = new GridBagConstraints();
        deliveryViewerConstraints.fill = GridBagConstraints.BOTH;
        deliveryViewerConstraints.weightx = 1;
        deliveryViewerConstraints.weighty = 1;
        deliveryViewerConstraints.insets = new Insets(0, 20, 5, 10);
        deliveryViewerConstraints.gridx = 2;
        deliveryViewerConstraints.gridy = 1;
        GridBagConstraints endOfBeamViewerConstraints = new GridBagConstraints();
        endOfBeamViewerConstraints.fill = GridBagConstraints.BOTH;
        endOfBeamViewerConstraints.weightx = 1;
        endOfBeamViewerConstraints.weighty = 1;
        endOfBeamViewerConstraints.insets = new Insets(0, 20, 0, 10);
        endOfBeamViewerConstraints.gridx = 2;
        endOfBeamViewerConstraints.gridy = 2;
        GridBagConstraints remainingTimeViewerConstraints = new GridBagConstraints();
        remainingTimeViewerConstraints.fill = GridBagConstraints.BOTH;
        remainingTimeViewerConstraints.weightx = 1;
        remainingTimeViewerConstraints.weighty = 1;
        remainingTimeViewerConstraints.insets = new Insets(5, 20, 0, 10);
        remainingTimeViewerConstraints.gridx = 2;
        remainingTimeViewerConstraints.gridy = 3;

        GridBagConstraints hLabelConstraints = new GridBagConstraints();
        hLabelConstraints.fill = GridBagConstraints.NONE;
        hLabelConstraints.weightx = 0;
        hLabelConstraints.weighty = 1;
        hLabelConstraints.insets = new Insets(0, 10, 0, 0);
        hLabelConstraints.gridx = 3;
        hLabelConstraints.gridy = 1;
        hLabelConstraints.anchor = GridBagConstraints.LINE_END;
        GridBagConstraints vLabelConstraints = new GridBagConstraints();
        vLabelConstraints.fill = GridBagConstraints.NONE;
        vLabelConstraints.weightx = 0;
        vLabelConstraints.weighty = 1;
        vLabelConstraints.insets = new Insets(0, 10, 0, 0);
        vLabelConstraints.gridx = 3;
        vLabelConstraints.gridy = 2;
        vLabelConstraints.anchor = GridBagConstraints.LINE_END;
        GridBagConstraints orbitLabelConstraints = new GridBagConstraints();
        orbitLabelConstraints.fill = GridBagConstraints.BOTH;
        orbitLabelConstraints.weightx = 1;
        orbitLabelConstraints.weighty = 1;
        orbitLabelConstraints.insets = new Insets(0, 0, 0, 0);
        orbitLabelConstraints.gridx = 4;
        orbitLabelConstraints.gridy = 0;
        GridBagConstraints emittanceLabelConstraints = new GridBagConstraints();
        emittanceLabelConstraints.fill = GridBagConstraints.BOTH;
        emittanceLabelConstraints.weightx = 1;
        emittanceLabelConstraints.weighty = 1;
        emittanceLabelConstraints.insets = new Insets(0, 0, 0, 0);
        emittanceLabelConstraints.gridx = 5;
        emittanceLabelConstraints.gridy = 0;
        GridBagConstraints tuneLabelConstraints = new GridBagConstraints();
        tuneLabelConstraints.fill = GridBagConstraints.BOTH;
        tuneLabelConstraints.weightx = 1;
        tuneLabelConstraints.weighty = 1;
        tuneLabelConstraints.insets = new Insets(0, 0, 0, 0);
        tuneLabelConstraints.gridx = 6;
        tuneLabelConstraints.gridy = 0;
        GridBagConstraints hOrbitViewerConstraints = new GridBagConstraints();
        hOrbitViewerConstraints.fill = GridBagConstraints.BOTH;
        hOrbitViewerConstraints.weightx = 1;
        hOrbitViewerConstraints.weighty = 1;
        hOrbitViewerConstraints.insets = new Insets(0, 0, 5, 5);
        hOrbitViewerConstraints.gridx = 4;
        hOrbitViewerConstraints.gridy = 1;
        GridBagConstraints vOrbitViewerConstraints = new GridBagConstraints();
        vOrbitViewerConstraints.fill = GridBagConstraints.BOTH;
        vOrbitViewerConstraints.weightx = 1;
        vOrbitViewerConstraints.weighty = 1;
        vOrbitViewerConstraints.insets = new Insets(0, 0, 5, 5);
        vOrbitViewerConstraints.gridx = 4;
        vOrbitViewerConstraints.gridy = 2;
        GridBagConstraints hEmittanceViewerConstraints = new GridBagConstraints();
        hEmittanceViewerConstraints.fill = GridBagConstraints.BOTH;
        hEmittanceViewerConstraints.weightx = 1;
        hEmittanceViewerConstraints.weighty = 1;
        hEmittanceViewerConstraints.insets = new Insets(0, 0, 5, 5);
        hEmittanceViewerConstraints.gridx = 5;
        hEmittanceViewerConstraints.gridy = 1;
        GridBagConstraints vEmittanceViewerConstraints = new GridBagConstraints();
        vEmittanceViewerConstraints.fill = GridBagConstraints.BOTH;
        vEmittanceViewerConstraints.weightx = 1;
        vEmittanceViewerConstraints.weighty = 1;
        vEmittanceViewerConstraints.insets = new Insets(0, 0, 5, 5);
        vEmittanceViewerConstraints.gridx = 5;
        vEmittanceViewerConstraints.gridy = 2;
        GridBagConstraints hTuneViewerConstraints = new GridBagConstraints();
        hTuneViewerConstraints.fill = GridBagConstraints.BOTH;
        hTuneViewerConstraints.weightx = 1;
        hTuneViewerConstraints.weighty = 1;
        hTuneViewerConstraints.insets = new Insets(0, 0, 5, 0);
        hTuneViewerConstraints.gridx = 6;
        hTuneViewerConstraints.gridy = 1;
        GridBagConstraints vTuneViewerConstraints = new GridBagConstraints();
        vTuneViewerConstraints.fill = GridBagConstraints.BOTH;
        vTuneViewerConstraints.weightx = 1;
        vTuneViewerConstraints.weighty = 1;
        vTuneViewerConstraints.insets = new Insets(0, 0, 5, 0);
        vTuneViewerConstraints.gridx = 6;
        vTuneViewerConstraints.gridy = 2;
        GridBagConstraints messageViewerConstraints = new GridBagConstraints();
        messageViewerConstraints.fill = GridBagConstraints.BOTH;
        messageViewerConstraints.weightx = 1;
        messageViewerConstraints.weighty = 1;
        messageViewerConstraints.insets = new Insets(0, 0, 0, 0);
        messageViewerConstraints.gridx = 4;
        messageViewerConstraints.gridy = 3;
        messageViewerConstraints.gridwidth = 3;

        centerPanel.setBackground(STANDARD_GRAY_BACKGROUND);

        centerPanel.add(getLogoLabel(), logoConstraints);
        centerPanel.add(boxPanel, boxConstraints);
        centerPanel.add(deliveryViewer, deliveryViewerConstraints);
        centerPanel.add(endOfBeamViewer, endOfBeamViewerConstraints);
        centerPanel.add(remainingTimeViewer, remainingTimeViewerConstraints);
        centerPanel.add(hLabel, hLabelConstraints);
        centerPanel.add(vLabel, vLabelConstraints);
        centerPanel.add(orbitRMSLabel, orbitLabelConstraints);
        centerPanel.add(emittanceLabel, emittanceLabelConstraints);
        centerPanel.add(tuneLabel, tuneLabelConstraints);
        centerPanel.add(hOrbitViewer, hOrbitViewerConstraints);
        centerPanel.add(vOrbitViewer, vOrbitViewerConstraints);
        centerPanel.add(hEmittanceViewer, hEmittanceViewerConstraints);
        centerPanel.add(vEmittanceViewer, vEmittanceViewerConstraints);
        centerPanel.add(hTuneViewer, hTuneViewerConstraints);
        centerPanel.add(vTuneViewer, vTuneViewerConstraints);
        centerPanel.add(messageViewer, messageViewerConstraints);

        return centerPanel;
    }

    private JLabel getLogoLabel() {
        if (logoLabel == null) {
            logoLabel = new JLabel(LOGO);
            logoLabel.setHorizontalAlignment(SwingConstants.LEFT);
            logoLabel.setVerticalAlignment(SwingConstants.CENTER);
        }
        return logoLabel;
    }

    private JPanel getSouthPanel() {
        southPanel = new JPanel(new GridBagLayout());

        GridBagConstraints operatorMessageDateConstraints = new GridBagConstraints();
        operatorMessageDateConstraints.fill = GridBagConstraints.BOTH;
        operatorMessageDateConstraints.weightx = 0;
        operatorMessageDateConstraints.weighty = 1;
        operatorMessageDateConstraints.insets = new Insets(0, 0, 2, 2);
        operatorMessageDateConstraints.gridx = 0;
        operatorMessageDateConstraints.gridy = 0;
        GridBagConstraints operatorMessageValueConstraints = new GridBagConstraints();
        operatorMessageValueConstraints.fill = GridBagConstraints.BOTH;
        operatorMessageValueConstraints.weightx = 1;
        operatorMessageValueConstraints.weighty = 1;
        operatorMessageValueConstraints.insets = new Insets(0, 0, 2, 0);
        operatorMessageValueConstraints.gridx = 1;
        operatorMessageValueConstraints.gridy = 0;
        operatorMessageValueConstraints.gridwidth = 2;
        GridBagConstraints operatorMessage2Constraints = new GridBagConstraints();
        operatorMessage2Constraints.fill = GridBagConstraints.BOTH;
        operatorMessage2Constraints.weightx = 1;
        operatorMessage2Constraints.weighty = 1;
        operatorMessage2Constraints.insets = new Insets(0, 0, 0, 0);
        operatorMessage2Constraints.gridx = 0;
        operatorMessage2Constraints.gridy = 1;
        operatorMessage2Constraints.gridwidth = 3;

        southPanel.setBackground(STANDARD_GRAY_BACKGROUND);

        southPanel.add(operatorMessageDateViewer, operatorMessageDateConstraints);
        southPanel.add(operatorMessageValueViewer, operatorMessageValueConstraints);
        southPanel.add(operatorMessage2Viewer, operatorMessage2Constraints);
        return southPanel;
    }

    public void updateChartData(String[] modes, double[] yValues, long[] times, double maxTrend, double tick,
            Map<String, Color> colors) {
        chart.updateChartData(modes, yValues, times, maxTrend, tick, colors);
    }

    public void disconnect() {
        IDataSourceProducer producer = DataSourceProducerProvider
                .getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
        if (producer instanceof TangoDataSourceFactory) {
            TangoDataSourceFactory factory = (TangoDataSourceFactory) producer;
            factory.removeRefreshingGroupListener(beamLinesGroupListener);
        }
        stringScalarBox.disconnectWidgetFromAll(currentViewer);
        stringScalarBox.disconnectWidgetFromAll(functionModeViewer);
        stringColorMBox.disconnectWidgetFromAll(frontEndStateReader);
        stringMBox.disconnectWidgetFromAll(stringTabReader);
        stringMBox.disconnectWidgetFromAll(string2TabReader);
        numberMBox.disconnectWidgetFromAll(numberTabReader);
        numberMBox.disconnectWidgetFromAll(number2TabReader);
        stringScalarBox.disconnectWidgetFromAll(fillingModeViewer);
        stringScalarBox.disconnectWidgetFromAll(lifetimeViewer);
        stringScalarBox.disconnectWidgetFromAll(currentTotalViewer);
        stringScalarBox.disconnectWidgetFromAll(averagePressureViewer);
        stringScalarBox.disconnectWidgetFromAll(deliveryViewer);
        stringScalarBox.disconnectWidgetFromAll(endOfBeamViewer);
        stringScalarBox.disconnectWidgetFromAll(remainingTimeViewer);
        stringScalarBox.disconnectWidgetFromAll(userInfoViewer);
        stringScalarBox.disconnectWidgetFromAll(hOrbitViewer);
        stringScalarBox.disconnectWidgetFromAll(vOrbitViewer);
        stringScalarBox.disconnectWidgetFromAll(hEmittanceViewer);
        stringScalarBox.disconnectWidgetFromAll(vEmittanceViewer);
        stringScalarBox.disconnectWidgetFromAll(hTuneViewer);
        stringScalarBox.disconnectWidgetFromAll(vTuneViewer);
        stringScalarBox.disconnectWidgetFromAll(messageViewer);
        stringScalarBox.disconnectWidgetFromAll(stringSpliter);
        stringScalarBox.disconnectWidgetFromAll(operatorMessage2Viewer);
        LOGGER.debug("bean targets disconnected !!!!");
    }

    public void connect() {
        keyCurrent = new TangoKey();
        keyFunctionMode = new TangoKey();
        keyFrontEndStateColor = new TangoKey();
        keyNameOfIDs = new TangoKey();
        keyStateOfIDs = new TangoKey();
        keyNameOfBendings = new TangoKey();
        keyStateOfBendings = new TangoKey();
        keyFillingMode = new TangoKey();
        keyLifetime = new TangoKey();
        keyCurrentTotal = new TangoKey();
        keyAveragePressure = new TangoKey();
        keyDelivery = new TangoKey();
        keyEndOfBeam = new TangoKey();
        keyRemainingTime = new TangoKey();
        keyUserInfo = new TangoKey();
        keyHOrbit = new TangoKey();
        keyVOrbit = new TangoKey();
        keyHEmittance = new TangoKey();
        keyVEmittance = new TangoKey();
        keyHTune = new TangoKey();
        keyVTune = new TangoKey();
        keyMessage = new TangoKey();
        keyOperatorMessage = new TangoKey();
        keyOperatorMessage2 = new TangoKey();

        // current
        TangoKeyTool.registerAttribute(keyCurrent, getModel(), CURRENT);
        TangoKeyTool.registerAttribute(keyFunctionMode, getModel(), FUNCTION_MODE);
        TangoKeyTool.registerAttribute(keyFillingMode, getModel(), FILLING_MODE);
        TangoKeyTool.registerAttribute(keyLifetime, getModel(), LIFETIME);
        TangoKeyTool.registerAttribute(keyCurrentTotal, getModel(), CURRENT_TOTAL);
        TangoKeyTool.registerAttribute(keyAveragePressure, getModel(), AVERAGE_PRESSURE);
        stringScalarBox.setColorEnabled(currentViewer, false);
        stringScalarBox.setUnitEnabled(currentViewer, true);
        stringScalarBox.connectWidget(currentViewer, keyCurrent);
        updateRefreshingStrategy(keyCurrent);
        stringScalarBox.setColorEnabled(functionModeViewer, false);
        stringScalarBox.connectWidget(functionModeViewer, keyFunctionMode);
        updateRefreshingStrategy(keyFunctionMode);
        stringScalarBox.setColorEnabled(fillingModeViewer, false);
        stringScalarBox.connectWidget(fillingModeViewer, keyFillingMode);
        updateRefreshingStrategy(keyFillingMode);
        stringScalarBox.setColorEnabled(lifetimeViewer, false);
        stringScalarBox.setUnitEnabled(lifetimeViewer, true);
        stringScalarBox.connectWidget(lifetimeViewer, keyLifetime);
        updateRefreshingStrategy(keyLifetime);
        stringScalarBox.setColorEnabled(currentTotalViewer, false);
        stringScalarBox.setUnitEnabled(currentTotalViewer, true);
        stringScalarBox.connectWidget(currentTotalViewer, keyCurrentTotal);
        updateRefreshingStrategy(keyCurrentTotal);
        stringScalarBox.setColorEnabled(averagePressureViewer, false);
        stringScalarBox.setUnitEnabled(averagePressureViewer, true);
        stringScalarBox.connectWidget(averagePressureViewer, keyAveragePressure, true, true, true);
        updateRefreshingStrategy(keyAveragePressure);

        // beamlines
        TangoKeyTool.registerAttribute(keyFrontEndStateColor, getModel(), FRONT_END_STATE_COLOR);
        stringColorMBox.connectWidget(frontEndStateReader, keyFrontEndStateColor);
        updateRefreshingStrategy(keyFrontEndStateColor);

        TangoKeyTool.registerAttribute(keyNameOfBendings, getModel(), NAME_OF_BENDINGS);
        TangoKeyTool.registerAttribute(keyStateOfBendings, getModel(), STATE_OF_BENDINGS);
        TangoKeyTool.registerAttribute(keyNameOfIDs, getModel(), NAME_OF_IDS);
        TangoKeyTool.registerAttribute(keyStateOfIDs, getModel(), STATE_OF_IDS);
        stringMBox.connectWidget(string2TabReader, keyNameOfBendings);
        updateRefreshingStrategy(keyNameOfBendings);
        numberMBox.connectWidget(number2TabReader, keyStateOfBendings);
        updateRefreshingStrategy(keyStateOfBendings);
        stringMBox.connectWidget(stringTabReader, keyNameOfIDs);
        updateRefreshingStrategy(keyNameOfIDs);
        numberMBox.connectWidget(numberTabReader, keyStateOfIDs);
        updateRefreshingStrategy(keyStateOfIDs);
        IDataSourceProducer producer = DataSourceProducerProvider
                .getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
        if (producer instanceof TangoDataSourceFactory) {
            TangoDataSourceFactory factory = (TangoDataSourceFactory) producer;
            factory.addRefreshingGroupListener(beamLinesGroupListener);
        }

        // beamInfo
        TangoKeyTool.registerAttribute(keyDelivery, getModel(), USABLE_SINCE);
        TangoKeyTool.registerAttribute(keyEndOfBeam, getModel(), NEXT_USER_BEAM);
        TangoKeyTool.registerAttribute(keyRemainingTime, getModel(), END_OF_CURRENT_FUNCTION_MODE);
        TangoKeyTool.registerAttribute(keyUserInfo, getModel(), USER_INFO);
        TangoKeyTool.registerAttribute(keyHOrbit, getModel(), H_RMS_ORBIT);
        TangoKeyTool.registerAttribute(keyVOrbit, getModel(), V_RMS_ORBIT);
        TangoKeyTool.registerAttribute(keyHEmittance, getModel(), H_EMITTANCE);
        TangoKeyTool.registerAttribute(keyVEmittance, getModel(), V_EMITTANCE);
        TangoKeyTool.registerAttribute(keyHTune, getModel(), H_TUNES);
        TangoKeyTool.registerAttribute(keyVTune, getModel(), V_TUNES);
        stringScalarBox.setColorEnabled(deliveryViewer, false);
        stringScalarBox.connectWidget(deliveryViewer, keyDelivery);
        updateRefreshingStrategy(keyDelivery);
        stringScalarBox.setColorEnabled(endOfBeamViewer, false);
        stringScalarBox.connectWidget(endOfBeamViewer, keyEndOfBeam);
        updateRefreshingStrategy(keyEndOfBeam);
        stringScalarBox.setColorEnabled(remainingTimeViewer, false);
        stringScalarBox.connectWidget(remainingTimeViewer, keyRemainingTime);
        updateRefreshingStrategy(keyRemainingTime);
        stringScalarBox.setColorEnabled(userInfoViewer, false);
        stringScalarBox.connectWidget(userInfoViewer, keyUserInfo);
        updateRefreshingStrategy(keyUserInfo);
        stringScalarBox.setColorEnabled(hOrbitViewer, false);
        stringScalarBox.setUnitEnabled(hOrbitViewer, true);
        stringScalarBox.connectWidget(hOrbitViewer, keyHOrbit);
        updateRefreshingStrategy(keyHOrbit);
        stringScalarBox.setColorEnabled(vOrbitViewer, false);
        stringScalarBox.setUnitEnabled(vOrbitViewer, true);
        stringScalarBox.connectWidget(vOrbitViewer, keyVOrbit);
        updateRefreshingStrategy(keyVOrbit);
        stringScalarBox.setColorEnabled(hEmittanceViewer, false);
        stringScalarBox.setUnitEnabled(hEmittanceViewer, true);
        stringScalarBox.connectWidget(hEmittanceViewer, keyHEmittance);
        updateRefreshingStrategy(keyHEmittance);
        stringScalarBox.setColorEnabled(vEmittanceViewer, false);
        stringScalarBox.setUnitEnabled(vEmittanceViewer, true);
        stringScalarBox.connectWidget(vEmittanceViewer, keyVEmittance);
        updateRefreshingStrategy(keyVEmittance);
        stringScalarBox.setColorEnabled(hTuneViewer, false);
        stringScalarBox.setUnitEnabled(hTuneViewer, true);
        stringScalarBox.connectWidget(hTuneViewer, keyHTune);
        updateRefreshingStrategy(keyHTune);
        stringScalarBox.setColorEnabled(vTuneViewer, false);
        stringScalarBox.setUnitEnabled(vTuneViewer, true);
        stringScalarBox.connectWidget(vTuneViewer, keyVTune);
        updateRefreshingStrategy(keyVTune);

        // message
        TangoKeyTool.registerAttribute(keyMessage, getModel(), MESSAGE);
        TangoKeyTool.registerAttribute(keyOperatorMessage, getModel(), OPERATOR_MESSAGE);
        TangoKeyTool.registerAttribute(keyOperatorMessage2, getModel(), OPERATOR_MESSAGE2);
        stringScalarBox.setColorEnabled(messageViewer, false);
        stringScalarBox.setUnitEnabled(messageViewer, true);
        stringScalarBox.connectWidget(messageViewer, keyMessage);
        updateRefreshingStrategy(keyMessage);
        stringScalarBox.setColorEnabled(operatorMessageDateViewer, false);
        stringScalarBox.setColorEnabled(operatorMessageValueViewer, false);
        stringScalarBox.connectWidget(stringSpliter, keyOperatorMessage);
        updateRefreshingStrategy(keyOperatorMessage);
        stringScalarBox.setColorEnabled(operatorMessage2Viewer, false);
        stringScalarBox.connectWidget(operatorMessage2Viewer, keyOperatorMessage2);
        updateRefreshingStrategy(keyOperatorMessage2);
    }

    public void updateRefreshingStrategy(IKey key) {
        if (key != null) {
            String attributeName = TangoKeyTool.getAttributeName(key);
            if (attributeName != null) {
                String group = GROUP_MAP.get(attributeName.toLowerCase());
                if (group != null) {
                    Integer period = periodMap.get(group);
                    if (period == null) {
                        period = Integer.valueOf(1000);
                        LOGGER.error("Period for the group {} is not well filled default value is used : 1000 ", group);
                    }
                    DataSourceProducerProvider.setRefreshingStrategy(key, new GroupRefreshingStrategy(group, period));
                } else {
                    LOGGER.error("The attribute {} has no refreshing group, so it is not polled ", attributeName);
                }
            }
        }
    }

    public MachineStatusChart getChart() {
        return chart;
    }

    // Update beamlines colors
    protected void updateLabels(BeamLineLabel[] labels, int[] codeId, String labelAttr, String stateAttr) {
        try {
            if ((labels != null) && (codeId != null)) {
                int length = Math.min(labels.length, codeId.length);
                for (int i = 0; i < length; i++) {
                    BeamLineLabel label = labels[i];
                    int code = codeId[i];
                    if (label != null) {
                        Color bg;
                        if (isColorFromAttr) {
                            bg = frontEndStateColors.get(Integer.valueOf(code)); // colors from device attribute
                        } else {
                            // default colors
                            bg = STATE_COLORS.get(Integer.valueOf(code));
                        }
                        if (bg == null) {
                            // Invalid state color applied
                            label.setBackground(FrontEndState.INVALID.getColor());
                            LOGGER.warn("WARNING: The statecolor code {} is undefined. Invalid state color applied.",
                                    code);
                        } else {
                            label.setBackground(bg);
                        }
                    } else {
                        LOGGER.debug("updateLabels labels[{}] is null", i);
                    }
                } // end for (int i = 0; i < length; i++)
                if (length < labels.length) {
                    // CONTROLGUI-369: Invalid state color applied for all labels that don't have a matching state code
                    LOGGER.warn(
                            "WARNING: {} has a bigger result than {} ({} VS {}), which means there are beamline labels with no matching state color",
                            labelAttr, stateAttr, labels.length, length);
                    for (int i = length; i < labels.length; i++) {
                        BeamLineLabel label = labels[i];
                        if (label != null) {
                            label.setBackground(FrontEndState.INVALID.getColor());
                        }
                    }
                }
            } else {
                LOGGER.debug("updateLabels else (labels != null) && (codeId != null)");
            }
        } catch (Exception e) {
            LOGGER.error("Exception raised in updateLabels {}", e);
        }
    }

    protected void computeAndLayoutBeamLinesLabels(JPanel targetPanel, String[] beamLineNames, boolean firstBeamLines) {
        if (targetPanel != null) {
            targetPanel.removeAll();
            targetPanel.setLayout(null);
            BeamLineLabel[] labels = firstBeamLines ? beamLinesLabels : beamLinesLabels2;
            BeamLineLabel[] formerLabels = labels;
            if (beamLineNames == null) {
                labels = new BeamLineLabel[0];
            } else {
                LOGGER.debug("computeAndLayoutBeamLinesLabels with firstBeamLines at {}", firstBeamLines);
                if (labels == null) {
                    labels = new BeamLineLabel[beamLineNames.length];
                } else {
                    labels = Arrays.copyOf(labels, beamLineNames.length);
                }
                int index = 0;
                // infraredPanel to null if exists
                if (!firstBeamLines) {
                    if (infraredPanel != null) {
                        infraredPanel.removeAll();
                        infraredPanel.setLayout(new GridLayout(1, 3, HGAP, VGAP));
                        infraredPanel.add(infraredLabel);
                    }
                }
                for (String name : beamLineNames) {
                    BeamLineLabel label = labels[index];
                    if (label == null) {
                        label = new BeamLineLabel(TDL_FONT, name);
                    } else {
                        label.forceText(name);
                        label.setBackground(FrontEndState.INVALID.getColor());
                    }
                    label.setComponentPopupMenu(feStateColorsPopupMenu);
                    labels[index++] = label;
                    // SMIS and AILES separation
                    if (!firstBeamLines && (SMIS.equals(name) || AILES.equals(name))) {
                        infraredPanel.add(label);
                    } else {
                        targetPanel.add(label);
                    }
                }
            }
            if (firstBeamLines) {
                beamLinesLabels = labels;
            } else {
                beamLinesLabels2 = labels;
            }
            if (formerLabels != null) {
                for (int i = labels.length; i < formerLabels.length; i++) {
                    formerLabels[i] = null;
                }
            }
            GridLayout layout;
            if ((labels == null) || (labels.length == 0)) {
                layout = new GridLayout();
            } else {
                if (firstBeamLines) {
                    int cols = 3;
                    int rows;
                    // (double) cast because we don't want euclidian division
                    rows = (int) Math.ceil(labels.length / (double) cols);
                    layout = new GridLayout(rows, cols, HGAP, VGAP);
                } else {
                    layout = new GridLayout(targetPanel.getComponentCount(), 1, HGAP, VGAP);
                }
                targetPanel.setLayout(layout);
                targetPanel.revalidate();
                revalidate();
                // TODO force update labels colors
                repaint();
            }
        }
    }

    public void startDateTimeUpdating() {

        DateUpdater dateUpdater = new DateUpdater(dateLabel, timeLabel);
        dateUpdater.actionPerformed(null);

        dateTimer = new Timer(1000, dateUpdater);
        dateTimer.start();
    }

    /**
     * Stop the updating of the display date and time
     */
    public void stopDateTimeUpdating() {

        if (isDateTimeUpdatingStarted()) {
            dateTimer.stop();
        }
    }

    /**
     * Is the date time updating started ?
     * 
     * @return true if the date time updating is started
     */
    public boolean isDateTimeUpdatingStarted() {
        return dateTimer != null && dateTimer.isRunning();
    }

    /**
     * Start the Screen Shoot of this bean
     */
    public void startScreenShooting() {
        // Create directories
        new File(directory).mkdirs();

        screenFile = new java.io.File(directory + "//" + fileName + "." + imageExtension);

        timer = new Timer(period * 1000, new ScreenShooting());
        timer.start();
    }

    /**
     * Stop the Screen Shoot of this bean
     */
    public void stopScreenShooting() {
        if (isScreenShootingStarted()) {
            timer.stop();
        }
    }

    /**
     * Is the screen shooting started ?
     * 
     * @return true if the screen shooting is started
     */
    public boolean isScreenShootingStarted() {
        return timer != null && timer.isRunning();
    }

    @Override
    public void clearGUI() {
        LOGGER.debug("clearGUI is called ");
        disconnecting = true;
        if (isScreenShootingStarted()) {
            stopScreenShooting();
        }
        disconnect();
        model.disconnectTargets();
        chart.clearChart();
        if (isDateTimeUpdatingStarted()) {
            stopDateTimeUpdating();
        }
    }

    @Override
    public void refreshGUI() {
        LOGGER.debug("refreshGUI is called ");
        disconnecting = false;
        if (!isDateTimeUpdatingStarted()) {
            startDateTimeUpdating();
        }
        model.setDeviceName(getModel());
        if (getModel() != null) {
            model.connectTargets();
            connect();
            if (isScreenshotEnabled) {
                startScreenShooting();
            }
        }
    }

    @Override
    protected void onConnectionError() {
        // Not used

    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // Not used

    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // Not used

    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private final class ScreenShooting implements ActionListener {
        /**
         * Action performed associated with this Action
         * 
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        @Override
        public void actionPerformed(ActionEvent evt) {
            try {

                // Get a screenshot
                BufferedImage shot = new BufferedImage(MachineStatusBean.this.getWidth(),
                        MachineStatusBean.this.getHeight(), BufferedImage.TYPE_INT_RGB);

                Graphics g = shot.getGraphics();
                MachineStatusBean.this.paint(g);
                g.dispose();

                // Save to remote file
                ImageIO.write(shot, imageExtension, screenFile);

            } catch (IOException e) {
                LOGGER.error("Connection error while writing Screenshot : ", e);
            } catch (Exception e) {
                LOGGER.error("Error while writing Screenshot : ", e);
            }
        }

    }

    // inner class for separating the message and its date
    protected class StringSpliter implements ITextTarget {

        private static final String SEPARATOR = " :";
        private static final String ERROR_WHILE_SETTING_OPERATOR_MESSAGE = "Error while setting operator message : {}";

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public String getText() {
            return null;
        }

        @Override
        public void setText(String text) {
            if (text != null && !text.isEmpty()) {
                try {
                    // text example : "Fri Jan 24 15:47 : Faisceau disponible"
                    int pos = text.indexOf(SEPARATOR);
                    if (pos > -1) {
                        messageDate = text.substring(0, pos); // the part before ":"
                        messageValue = text.substring(text.indexOf(SEPARATOR) + 2); // the part after ":"
                        operatorMessageDateViewer
                                .setText(IDateConstants.SPACE_SEPARATOR + messageDate + IDateConstants.SPACE_SEPARATOR);
                        operatorMessageValueViewer.setText(messageValue);
                    }
                } catch (Exception e) {
                    LOGGER.error(ERROR_WHILE_SETTING_OPERATOR_MESSAGE, text);
                }
            }
        }
    }

    // -----------------------------------------------------------------------------------------
    //
    // VIRTUAL TARGETS
    //
    // -----------------------------------------------------------------------------------------

    // String reader to get one dimension array FOR NAMES OF BEAMLINES !!!
    protected class StringTabReader extends TextMatrixTargetAdapter {

        private static final String NAME_OF_I_DS_NEW_VALUE_IS_NULL = "nameOfIDs new value is null";

        @Override
        public void setStringMatrix(String[][] value) {
            if (value == null) {
                setFlatStringMatrix(null, 0, 0);
            } else {
                String[] array = (String[]) ArrayUtils.convertArrayDimensionFromNTo1(value);
                setFlatStringMatrix(array, array.length, 1);
            }
        }

        @Override
        public void setFlatStringMatrix(String[] value, int width, int height) {
            // TODO
            // if length is over 12, its Insertion Devices
            if (value != null) {
                if (ObjectUtils.sameObject(beamLinesTab, value)) {
                    beamLinesTabChanged = false;
                } else {
                    beamLinesTab = value;
                    beamLinesTabChanged = true;
                }
            } else if (!disconnecting) {
                LOGGER.error(NAME_OF_I_DS_NEW_VALUE_IS_NULL);
            }
        }

    }

    protected class String2TabReader extends TextMatrixTargetAdapter {

        private static final String NAME_OF_BENDINGS_NEW_VALUE_IS_NULL = "nameOfBendings new value is null";

        @Override
        public void setStringMatrix(String[][] value) {
            if (value == null) {
                setFlatStringMatrix(null, 0, 0);
            } else {
                String[] array = (String[]) ArrayUtils.convertArrayDimensionFromNTo1(value);
                setFlatStringMatrix(array, array.length, 1);
            }
        }

        @Override
        public void setFlatStringMatrix(String[] value, int width, int height) {
            if (value != null) {
                if (ObjectUtils.sameObject(beamLinesTab2, value)) {
                    beamLinesTab2Changed = false;
                } else {
                    beamLinesTab2 = value;
                    beamLinesTab2Changed = true;
                }
            } else if (!disconnecting) {
                LOGGER.error(NAME_OF_BENDINGS_NEW_VALUE_IS_NULL);
            }
        }

    }

    // reader for BEAMLINES STATES (to convert into colors)
    protected class NumberTabReader extends NumberMatrixTargetAdapter {

        private static final String INSERTION_DEVICE_VALUE_AND_NEW_VALUE_ARE_SAME_OBJECT = "Insertion Device value and new value are same object";
        private static final String STATE_CODE_ID_VALUE = "stateCodeID value {} ";
        private static final String INSERTION_DEVICE_NEW_VALUE_AFTER_EXTRACTION_IS_NULL_OR_EMPTY = "Insertion Device new value after extraction is null or empty";
        private static final String INSERTION_DEVICE_NEW_VALUE_IS_NULL_STATE_OF_I_DS = "Insertion Device new value is null (stateOfIDs)";

        @Override
        public void setNumberMatrix(Object[] value) {
            if (value == null) {
                setFlatNumberMatrix(null, 0, 0);
            } else {
                Object array = ArrayUtils.convertArrayDimensionFromNTo1(value);
                setFlatNumberMatrix(array, 1, 1);
            }
        }

        @Override
        public void setFlatNumberMatrix(Object value, int width, int height) {
            if (value != null) {
                int[] tmp = NumberArrayUtils.extractIntArray(value);
                if (tmp != null && tmp.length > 0) {
                    if (ObjectUtils.sameObject(stateCodeID, tmp)) {
                        stateCodeIDChanged = false;
                        LOGGER.error(INSERTION_DEVICE_VALUE_AND_NEW_VALUE_ARE_SAME_OBJECT);
                    } else {
                        LOGGER.debug(STATE_CODE_ID_VALUE, Arrays.toString(tmp));
                        stateCodeID = tmp;
                        stateCodeIDChanged = true;
                    }
                } else {
                    stateCodeID = null;
                    LOGGER.error(INSERTION_DEVICE_NEW_VALUE_AFTER_EXTRACTION_IS_NULL_OR_EMPTY);
                }

            } else if (!disconnecting) {
                stateCodeID = null;
                LOGGER.error(INSERTION_DEVICE_NEW_VALUE_IS_NULL_STATE_OF_I_DS);
            }
        }
    }

    protected class Number2TabReader extends NumberMatrixTargetAdapter {

        private static final String BENDING_MAGNET_VALUE_AND_NEW_VALUE_ARE_SAME_OBJECT = "Bending Magnet value and new value are same object";
        private static final String STATE_CODE_BM_VALUE = "stateCodeBM value {} ";
        private static final String BENDING_MAGNET_NEW_VALUE_AFTER_EXTRACTION_IS_NULL_OR_EMPTY = "Bending Magnet new value after extraction is null or empty";
        private static final String BENDING_MAGNET_NEW_VALUE_IS_NULL_STATE_OF_BENDINGS = "BendingMagnet new value is null (stateOfBendings) ";

        @Override
        public void setNumberMatrix(Object[] value) {
            if (value == null) {
                setFlatNumberMatrix(null, 0, 0);
            } else {
                Object array = ArrayUtils.convertArrayDimensionFromNTo1(value);
                setFlatNumberMatrix(array, 1, 1);
            }
        }

        @Override
        public void setFlatNumberMatrix(Object value, int width, int height) {
            if (value != null) {
                int[] tmp = NumberArrayUtils.extractIntArray(value);
                if (tmp != null && tmp.length > 0) {
                    if (ObjectUtils.sameObject(stateCodeBM, tmp)) {
                        stateCodeBMChanged = false;
                        LOGGER.error(BENDING_MAGNET_VALUE_AND_NEW_VALUE_ARE_SAME_OBJECT);
                    } else {
                        LOGGER.debug(STATE_CODE_BM_VALUE, Arrays.toString(tmp));
                        stateCodeBM = tmp;
                        stateCodeBMChanged = true;
                    }
                } else {
                    stateCodeBM = tmp;
                    LOGGER.error(BENDING_MAGNET_NEW_VALUE_AFTER_EXTRACTION_IS_NULL_OR_EMPTY);
                }
            } else if (!disconnecting) {
                stateCodeBM = null;
                LOGGER.error(BENDING_MAGNET_NEW_VALUE_IS_NULL_STATE_OF_BENDINGS);
            }
        }
    }

    // reads color rgb codes from device attribute
    protected class FrontEndStateReader extends TextMatrixTargetAdapter {

        private static final String FRONT_END_STATE_READER_SET_FLAT_STRING_MATRIX = "FrontEndStateReader.setFlatStringMatrix {} ";
        private static final String FRONT_END_STATE_COLORS_FROM_DEVICE_ATTRIBUTE_ARE_INVALID_DEFAULT_COLORS_APPLIED_FOR_BEAMLINES = "FrontEndStateColors from device attribute are invalid. Default colors applied for beamlines.";
        private static final String FRONT_END_STATE_COLORS_ATTRIBUTE_LENGTH_IS_EMPTY_DEFAULT_COLORS_APPLIED_FOR_BEAMLINES = "FrontEndStateColors attribute length is empty. Default colors applied for beamlines.";
        private static final String FRONT_END_STATE_READER_SET_FLAT_STRING_MATRIX_RECEIVED_VALUE_IS_NULL_DEFAULT_COLORS_USAGE = "FrontEndStateReader.setFlatStringMatrix received value is null, default colors usage";

        @Override
        public void setStringMatrix(String[][] value) {
            if (value == null) {
                setFlatStringMatrix(null, 0, 0);
            } else {
                String[] array = (String[]) ArrayUtils.convertArrayDimensionFromNTo1(value);
                setFlatStringMatrix(array, array.length, 1);
            }
        }

        @Override
        public void setFlatStringMatrix(String[] value, int width, int height) {
            if (value != null) {
                if (!ObjectUtils.sameObject(frontColorsMemoryTab, value)) {
                    int r = 0, g = 0, b = 0;
                    String[] set;
                    String[] rgb;
                    LOGGER.debug(FRONT_END_STATE_READER_SET_FLAT_STRING_MATRIX, Arrays.toString(value));
                    if (value.length > 0) {
                        for (String s : value) {// parsing property values to extract RGB numbers and compose colors
                            try {
                                set = s.split(IDateConstants.TIME_SEPARATOR);
                                rgb = set[1].split(CometeConstants.COMMA);
                                r = Integer.parseInt(rgb[0].trim());
                                g = Integer.parseInt(rgb[1].trim());
                                b = Integer.parseInt(rgb[2].trim());
                                frontEndStateColors.put(Integer.parseInt(set[0].trim()), new Color(r, g, b));
                                isColorFromAttr = true;
                            } catch (Exception e) {
                                isColorFromAttr = false;
                                LOGGER.error(
                                        FRONT_END_STATE_COLORS_FROM_DEVICE_ATTRIBUTE_ARE_INVALID_DEFAULT_COLORS_APPLIED_FOR_BEAMLINES);
                                break;
                            }
                        }
                    } else {
                        isColorFromAttr = false;
                        LOGGER.error(
                                FRONT_END_STATE_COLORS_ATTRIBUTE_LENGTH_IS_EMPTY_DEFAULT_COLORS_APPLIED_FOR_BEAMLINES);
                    }
                    if (isColorFromAttr) {
                        frontColorsMemoryTab = value;
                    }
                }
            } else if (!disconnecting) { // value is null !!!
                isColorFromAttr = false;
                frontColorsMemoryTab = value;
                LOGGER.error(FRONT_END_STATE_READER_SET_FLAT_STRING_MATRIX_RECEIVED_VALUE_IS_NULL_DEFAULT_COLORS_USAGE);
            }
        }

    }

    // -----------------------------------------------------------------------------------------
    //
    // END OF VIRTUAL TARGETS
    //
    // -----------------------------------------------------------------------------------------

    private final class DateUpdater implements ActionListener {

        private static final String DATE_FORMAT = "dd/MM/yyyy";

        private JLabel date;
        private JLabel time;
        private DateFormat dateFormat;
        private DateFormat timeFormat;

        public DateUpdater(JLabel dateLabel, JLabel timeLabel) {
            Locale currentLocale = Locale.FRANCE;
            date = dateLabel;
            time = timeLabel;
            timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, currentLocale);
            dateFormat = new SimpleDateFormat(DATE_FORMAT);
        }

        @Override
        public void actionPerformed(ActionEvent arg0) {
            if ((date != null) && (time != null)) {
                Date newDate = new Date();
                date.setText(dateFormat.format(newDate));
                time.setText(timeFormat.format(newDate));
            }
        }

    }

    // ////////////////////////// //
    // Refreshing group listeners //
    // ////////////////////////// //

    protected class BeamLinesGroupListener implements IRefreshingGroupListener {

        public BeamLinesGroupListener() {
            super();
        }

        @Override
        public void groupRefreshed(GroupEvent event) {
            if (BEAMLINES_GROUP.equals(event.getGroupName())
                    && (stateCodeIDChanged || stateCodeBMChanged || beamLinesTabChanged || beamLinesTab2Changed)) {
                boolean stateCodeIDChanged = MachineStatusBean.this.stateCodeIDChanged;
                MachineStatusBean.this.stateCodeIDChanged = false;
                boolean stateCodeBMChanged = MachineStatusBean.this.stateCodeBMChanged;
                MachineStatusBean.this.stateCodeBMChanged = false;
                boolean beamLinesTabChanged = MachineStatusBean.this.beamLinesTabChanged;
                MachineStatusBean.this.beamLinesTabChanged = false;
                boolean beamLinesTab2Changed = MachineStatusBean.this.beamLinesTab2Changed;
                MachineStatusBean.this.beamLinesTab2Changed = false;
                String[] beamLinesTab = MachineStatusBean.this.beamLinesTab;
                String[] beamLinesTab2 = MachineStatusBean.this.beamLinesTab2;
                int[] stateCodeID = MachineStatusBean.this.stateCodeID;
                int[] stateCodeBM = MachineStatusBean.this.stateCodeBM;
                SwingUtilities.invokeLater(() -> {
                    if (beamLinesTabChanged) {
                        computeAndLayoutBeamLinesLabels(insertionBeamPanel, beamLinesTab, true);
                    }
                    if (beamLinesTab2Changed) {
                        computeAndLayoutBeamLinesLabels(bendingBeamPanel, beamLinesTab2, false);
                    }
                    if (stateCodeIDChanged || beamLinesTabChanged) {
                        updateLabels(beamLinesLabels, stateCodeID, NAME_OF_IDS, STATE_OF_IDS);
                    }
                    if (stateCodeBMChanged || beamLinesTab2Changed) {
                        updateLabels(beamLinesLabels2, stateCodeBM, NAME_OF_BENDINGS, STATE_OF_BENDINGS);
                    }
                });
            }
        }

    }

}
