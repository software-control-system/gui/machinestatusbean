package fr.soleil.bean.machinestatus.model;

import java.awt.Color;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.machinestatus.MachineStatusBean;
import fr.soleil.bean.machinestatus.customtargets.NumberMatrixTargetAdapter;
import fr.soleil.bean.machinestatus.customtargets.NumberTargetAdapter;
import fr.soleil.bean.machinestatus.customtargets.TextMatrixTargetAdapter;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.event.GroupEvent;
import fr.soleil.data.listener.IRefreshingGroupListener;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;

public class MachineStatusModel implements IMachineStatusConstants, IModelConnector, IRefreshingGroupListener {

    protected static final String[] NO_MODE = new String[0];
    protected static final double[] NO_VALUE = new double[0];
    protected static final long[] NO_TIME = new long[0];

    private final Logger logger;
    private final MachineStatusBean view;
    private final MaxTrendReader maxTrendReader;
    private final TickSpacingReader tickSpacingReader;
    private final CurrentTrendReader currentTrendReader;
    private final CurrentTrendTimesReader currentTrendTimesReader;
    private final ModesReader modesReader;
    private final DefaultModesColorReader defaultModesColorReader;
    private volatile String[] modes;
    private volatile long[] times;
    private volatile double[] yValues;
    private double maxTrendValue;
    private double tickSpacing;
    private String[] modesColorsMemoryTab;
    private final Map<String, Color> chartAttributeColors;
    private String deviceName;
    private final NumberScalarBox numberScalarBox;
    private final StringMatrixBox stringMBox;
    private final NumberMatrixBox numberMBox;
    private long refreshDateRefForChart;
    private volatile boolean modesChanged, yValuesChanged, timesChanged, maxTrendValueChanged, tickSpacingChanged,
            chartAttributeColorsChanged;
    private volatile boolean modesAdded, yValuesAdded, timesAdded;

    public MachineStatusModel(MachineStatusBean bean) {
        view = bean;
        logger = LoggerFactory.getLogger(MachineStatusBean.class);
        chartAttributeColors = new ConcurrentHashMap<String, Color>();
        numberScalarBox = new NumberScalarBox();
        stringMBox = new StringMatrixBox();
        numberMBox = new NumberMatrixBox();
        currentTrendReader = new CurrentTrendReader();
        currentTrendTimesReader = new CurrentTrendTimesReader();
        modesReader = new ModesReader();
        maxTrendReader = new MaxTrendReader();
        tickSpacingReader = new TickSpacingReader();
        defaultModesColorReader = new DefaultModesColorReader();
        refreshDateRefForChart = -1;

        resetData();
        updateChart();
    }

    protected void resetData() {
        modes = NO_MODE;
        yValues = NO_VALUE;
        times = NO_TIME;
        modesChanged = true;
        yValuesChanged = true;
        timesChanged = true;
        maxTrendValueChanged = false;
        tickSpacingChanged = false;
        chartAttributeColorsChanged = false;

        modesAdded = false;
        yValuesAdded = false;
        timesAdded = false;
    }

    @Override
    public void connectTargets() {
        TangoKey keyTrend = new TangoKey();
        TangoKey keyTimes = new TangoKey();
        TangoKey keyModes = new TangoKey();
        TangoKey keyMaxTrend = new TangoKey();
        TangoKey keyTick = new TangoKey();
        TangoKey keyDefaultModesColor = new TangoKey();
        TangoKeyTool.registerAttribute(keyTrend, deviceName, CURRENT_TREND);
        TangoKeyTool.registerAttribute(keyTimes, deviceName, CURRENT_TREND_TIMES);
        TangoKeyTool.registerAttribute(keyModes, deviceName, FUNCTION_MODE_TREND);
        TangoKeyTool.registerAttribute(keyMaxTrend, deviceName, MAX_TREND_VALUE);
        TangoKeyTool.registerAttribute(keyTick, deviceName, TICK_SPACING);
        TangoKeyTool.registerAttribute(keyDefaultModesColor, deviceName, DEFAULT_MODES_COLOR);

        view.getChart().setKeyInfo(keyTrend.getInformationKey());
        view.getChart().initChartConfiguration(view.isChartSampled());
        resetData();
        updateChart();

        numberScalarBox.connectWidget(maxTrendReader, keyMaxTrend);
        view.updateRefreshingStrategy(keyMaxTrend);
        numberScalarBox.connectWidget(tickSpacingReader, keyTick);
        view.updateRefreshingStrategy(keyTick);
        numberMBox.connectWidget(currentTrendReader, keyTrend);
        view.updateRefreshingStrategy(keyTrend);
        numberMBox.connectWidget(currentTrendTimesReader, keyTimes);
        view.updateRefreshingStrategy(keyTimes);
        stringMBox.connectWidget(modesReader, keyModes);
        view.updateRefreshingStrategy(keyModes);
        stringMBox.connectWidget(defaultModesColorReader, keyDefaultModesColor);
        view.updateRefreshingStrategy(keyDefaultModesColor);

        IDataSourceProducer producer = DataSourceProducerProvider
                .getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
        if (producer instanceof AbstractRefreshingManager<?>) {
            ((AbstractRefreshingManager<?>) producer).addRefreshingGroupListener(this);
        }
    }

    @Override
    public void disconnectTargets() {
        IDataSourceProducer producer = DataSourceProducerProvider
                .getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
        if (producer instanceof AbstractRefreshingManager<?>) {
            ((AbstractRefreshingManager<?>) producer).removeRefreshingGroupListener(this);
        }

        numberScalarBox.disconnectWidgetFromAll(maxTrendReader);
        numberScalarBox.disconnectWidgetFromAll(tickSpacingReader);
        numberMBox.disconnectWidgetFromAll(currentTrendReader);
        numberMBox.disconnectWidgetFromAll(currentTrendTimesReader);
        stringMBox.disconnectWidgetFromAll(modesReader);
        stringMBox.disconnectWidgetFromAll(defaultModesColorReader);

        resetData();
        updateChart();
        logger.debug("chart targets disconnected !!!!");
    }

    private class MaxTrendReader extends NumberTargetAdapter {

        @Override
        public void setNumberValue(Number arg0) {

            if (arg0 != null) {
                double newValue = arg0.doubleValue();
                maxTrendValueChanged = (maxTrendValue != newValue);
                maxTrendValue = arg0.doubleValue();
            } else if (!view.isDisconnecting()) {
                maxTrendValueChanged = false;
                logger.error("maxTrendValue value is null");
            }
        }

    }

    protected class TickSpacingReader extends NumberTargetAdapter {
        @Override
        public void setNumberValue(Number arg0) {

            if (arg0 != null) {
                double newValue = arg0.doubleValue();
                tickSpacingChanged = (tickSpacing != newValue);
                tickSpacing = newValue;
            } else if (!view.isDisconnecting()) {
                tickSpacingChanged = false;
                logger.error("tickSpacing value is null");
            }
        }

    }

    // reads chart colors from device attribute
    protected class DefaultModesColorReader extends TextMatrixTargetAdapter {

        @Override
        public void setStringMatrix(String[][] value) {
            if (value == null) {
                setFlatStringMatrix(null, 0, 0);
            } else {
                String[] array = (String[]) ArrayUtils.convertArrayDimensionFromNTo1(value);
                setFlatStringMatrix(array, array.length, 1);
            }
        }

        @Override
        public void setFlatStringMatrix(String[] value, int width, int height) {
            if (value != null) {
                if (ObjectUtils.sameObject(modesColorsMemoryTab, value)) {
                    chartAttributeColorsChanged = false;
                } else {
                    if (value.length == 6) {
                        modesColorsMemoryTab = value;
                    }
                    int r = 0, g = 0, b = 0;
                    String[] set;
                    String[] rgb;
                    logger.debug("DefaultModesColor.setFlatStringMatrix {} ", Arrays.toString(value));
                    // 6 is the number of official states for function modes colors
                    if (value.length == 6 && value[0].length() > 0) {
                        for (String s : value) {// parsing property values to extract RGB numbers and compose colors
                            try {
                                set = s.split(":");
                                rgb = set[1].split(",");
                                r = Integer.parseInt(rgb[0]);
                                g = Integer.parseInt(rgb[1]);
                                b = Integer.parseInt(rgb[2]);
                                Color col = new Color(r, g, b);
                                chartAttributeColors.put(set[0], col);
                                chartAttributeColorsChanged = true;
                            } catch (Exception e) {
                                logger.warn("defaultModesColor from device attribute are invalid."
                                        + " Default colors applicated for the chart.");
                                chartAttributeColors.clear();
                                chartAttributeColorsChanged = true;
                                break;
                            }
                            // System.out.println(set[0] + "--> " + rgb[0] + " " + rgb[1] + " " + rgb[2]);
                        }
                    } else if (value.length != 6) {
                        logger.warn("defaultModesColor attribute length is incorrect."
                                + " Default colors applicated for the chart.");
                        chartAttributeColorsChanged = true;
                        chartAttributeColors.clear();
                    }
                }
            } else if (!view.isDisconnecting()) {
                chartAttributeColorsChanged = false;
                logger.error("defaultModesColor is null.");
            }
        }

    }

    // lecteur des données en X du chart (les heures !)
    private class CurrentTrendTimesReader extends NumberMatrixTargetAdapter {

        @Override
        public void setNumberMatrix(Object[] value) {
            if (value == null) {
                setFlatNumberMatrix(null, 0, 0);
            } else {
                Object array = ArrayUtils.convertArrayDimensionFromNTo1(value);
                setFlatNumberMatrix(array, 1, 1);
            }
        }

        @Override
        public void setFlatNumberMatrix(Object value, int width, int height) {
            if ((value == null) && (!timesAdded)) {
                value = NO_TIME;
            }
            if (value != null) {
                long[] values = NumberArrayUtils.extractLongArray(value);
                if (values.length > 0) {
                    timesAdded = true;
                }
                if (ObjectUtils.sameObject(values, times)) {
                    timesChanged = false;
                } else {
                    times = values;
                    timesChanged = true;
                }
            } else if (!view.isDisconnecting()) {
                timesChanged = false;
                logger.error("currentTrendTimes value is null");
            }
        }
    }

    // lecteur des données en Y du chart
    private class CurrentTrendReader extends NumberMatrixTargetAdapter {

        @Override
        public void setNumberMatrix(Object[] value) {
            if (value == null) {
                setFlatNumberMatrix(null, 0, 0);
            } else {
                Object array = ArrayUtils.convertArrayDimensionFromNTo1(value);
                setFlatNumberMatrix(array, 1, 1);
            }
        }

        @Override
        public void setFlatNumberMatrix(Object value, int width, int height) {
            if ((value == null) && (!yValuesAdded)) {
                value = NO_VALUE;
            }
            if (value != null) {
                double[] values = NumberArrayUtils.extractDoubleArray(value);
                if (values.length > 0) {
                    yValuesAdded = true;
                }
                if (ObjectUtils.sameObject(values, yValues)) {
                    yValuesChanged = false;
                } else {
                    yValues = values;
                    yValuesChanged = true;
                }
            } else if (!view.isDisconnecting()) {
                yValuesChanged = false;
                logger.error("currentTrend value is null");
            }
        }

    }

    // lecteur des functionModeTrend
    private class ModesReader extends TextMatrixTargetAdapter {
        @Override
        public void setStringMatrix(String[][] value) {
            try {
                if (value == null) {
                    setFlatStringMatrix(null, 0, 0);
                } else {
                    setFlatStringMatrix((String[]) ArrayUtils.convertArrayDimensionFromNTo1(value), 0, 0);
                }
            } catch (Exception e) {
                logger.error("ModeReader exeception {}", e);
//                e.printStackTrace();
            }
        }

        @Override
        public void setFlatStringMatrix(String[] value, int width, int height) {
            if ((value == null) && (!modesAdded)) {
                value = NO_MODE;
            }
            if (value != null) {
                if (value.length > 0) {
                    modesAdded = true;
                }
                if (ObjectUtils.sameObject(value, modes)) {
                    modesChanged = false;
                } else {
                    modes = value;
                    modesChanged = true;
                }
            } else if (!view.isDisconnecting()) {
                modesChanged = false;
                logger.error("functionModeTrend value is null");
            }
        }

    }

    private void updateChart() {
        modesChanged = false;
        yValuesChanged = false;
        timesChanged = false;
        maxTrendValueChanged = false;
        tickSpacingChanged = false;
        chartAttributeColorsChanged = false;
        String[] modes = this.modes;
        double[] yValues = this.yValues;
        long[] times = this.times;
        if ((modes != null) && (yValues != null) && (times != null)) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - refreshDateRefForChart >= view.getTrendGroupPeriod()) {
                refreshDateRefForChart = currentTime;
                view.updateChartData(modes, yValues, times, maxTrendValue, tickSpacing, chartAttributeColors);
            }
        } else {
            logger.debug("updateChart is called but at least one value is null modes {}, times {} ..... ",
                    modes == null, times == null);
            logger.debug("..... and yValues {}", yValues == null);
        }
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    @Override
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;

    }

    @Override
    public void groupRefreshed(GroupEvent event) {
        if ((event != null) && MachineStatusBean.TREND_GROUP.equals(event.getGroupName())
                && (modesChanged || yValuesChanged || timesChanged || maxTrendValueChanged || tickSpacingChanged
                        || chartAttributeColorsChanged)) {
            updateChart();
        }
    }
}
