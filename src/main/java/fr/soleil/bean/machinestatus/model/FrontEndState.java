package fr.soleil.bean.machinestatus.model;

import java.awt.Color;

// Front End state colors and meanings as defined in CTRLRFC-503
public enum FrontEndState implements IColorConstant, IHtmlConstant {

    // 0: dark gray --> FE does not answer (Tango problem)
    INVALID(0,
            HTML_TITLE_START + "Failed to communicate with Front End" + HTML_DESCRIPTION_SEPARATOR
                    + " (tango communication error)" + HTML_DESCRIPTION_END,
            STANDARD_GRAY_BACKGROUND),
    // 5: gray --> unknown state
    UNKNOWN(5,
            HTML_TITLE_START + "Unknown" + HTML_DESCRIPTION_SEPARATOR + " (only for SMIS and AILES)"
                    + HTML_DESCRIPTION_END,
            new Color(220, 220, 220)),
    // 10: red --> FE closed with problem
    FE_PROBLEM(10, HTML_TITLE_START + "Front End problem" + HTML_TITLE_END, new Color(255, 100, 100)),
    // 20: magenta --> FE locked
    FE_LOCKED(20, HTML_TITLE_START + "Front End locked" + HTML_TITLE_END, new Color(255, 150, 230)),
    // 30: yellow --> FE standby
    FE_STANDBY(30, HTML_TITLE_START + "Front End standby" + HTML_TITLE_END, new Color(255, 255, 100)),
    // 35: orange --> FE closed alarm
    FE_ALARM(35, HTML_TITLE_START + "Front End alarm" + HTML_TITLE_END, new Color(255, 153, 0)),
    // 40: white --> FE closed
    FE_CLOSED(40, HTML_TITLE_START + "Front End closed" + HTML_TITLE_END, Color.WHITE),
    // 45: light blue --> FE half opened
    FE_HALF_OPENED(45,
            HTML_TITLE_START + "Front End half opened" + HTML_DESCRIPTION_SEPARATOR
                    + "<br />(only for ROCK, SMIS, PUMA, NANOSCOPIUM and ANATOMIX)" + HTML_DESCRIPTION_END,
            new Color(58, 222, 255)),
    // 47: dark blue --> FE partially opened special case
    FE_PARTIAL_OPENED(47,
            HTML_TITLE_START + "Front End partially opened:" + HTML_DESCRIPTION_SEPARATOR
                    + "<ul><li>ROCK: half opened (automatic mode)</li>" + "<li>NANOSCOPIUM: 3/4 opened</li>"
                    + "<li>ANATOMIX: 3/4 opened</li></ul>" + HTML_DESCRIPTION_END,
            new Color(42, 145, 255)),
    // 50: green --> FE opened
    FE_OPENED(50, HTML_TITLE_START + "Front End opened" + HTML_TITLE_END, new Color(120, 255, 120)),
    // 60: dark green --> FE opened automatic mode
    FE_OPENED_AUTO(60, HTML_TITLE_START + "Front End opened" + HTML_DESCRIPTION_SEPARATOR + " (automatic mode)"
            + HTML_DESCRIPTION_END, new Color(50, 200, 50));

    private final int value;
    private final String name;
    private final Color color;

    private FrontEndState(int value, String name, Color color) {
        this.value = value;
        this.name = name;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

}
