package fr.soleil.bean.machinestatus.model;

public interface IHtmlConstant {
    public static final String HTML_TITLE_START = "<html><boddy><span style=\"font-size:12px\">";
    public static final String HTML_TITLE_END = "</span></body></html>";
    public static final String HTML_DESCRIPTION_SEPARATOR = "</span> <i>";
    public static final String HTML_DESCRIPTION_END = "</i></body></html>";
    public static final String HTML_END = "</body></html>";
}
