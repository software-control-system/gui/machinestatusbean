package fr.soleil.bean.machinestatus.model;

public interface IMachineStatusConstants {

    // attribute groups
    public static final String CURRENT_GROUP = "current";
    public static final String BEAMLINES_GROUP = "beamLines";
    public static final String BEAM_INFO_GROUP = "beamInfo";
    public static final String TREND_GROUP = "trend";
    public static final String MESSAGE_GROUP = "message";

    // attributes
    public static final String CURRENT = "current";
    public static final String FUNCTION_MODE = "functionMode";
    public static final String FILLING_MODE = "fillingMode";
    public static final String LIFETIME = "lifetime";
    public static final String CURRENT_TOTAL = "currentTotal";
    public static final String AVERAGE_PRESSURE = "averagePressure";
    public static final String NAME_OF_BENDINGS = "nameOfBendings";
    public static final String STATE_OF_BENDINGS = "stateOfBendings";
    public static final String NAME_OF_IDS = "nameOfIDs";
    public static final String STATE_OF_IDS = "stateOfIDs";
    public static final String FRONT_END_STATE_COLOR = "frontEndStateColor";
    public static final String USABLE_SINCE = "usableSince";
    public static final String NEXT_USER_BEAM = "nextUserBeam";
    public static final String END_OF_CURRENT_FUNCTION_MODE = "endOfCurrentFunctionMode";
    public static final String USER_INFO = "userInfo";
    public static final String H_RMS_ORBIT = "hRMSOrbit";
    public static final String V_RMS_ORBIT = "vRMSOrbit";
    public static final String H_EMITTANCE = "hEmittance";
    public static final String V_EMITTANCE = "vEmittance";
    public static final String H_TUNES = "hTunes";
    public static final String V_TUNES = "vTunes";
    public static final String MESSAGE = "message";
    public static final String OPERATOR_MESSAGE = "operatorMessage";
    public static final String OPERATOR_MESSAGE2 = "operatorMessage2";
    public static final String CURRENT_TREND = "currentTrend";
    public static final String CURRENT_TREND_TIMES = "currentTrendTimes";
    public static final String FUNCTION_MODE_TREND = "functionModeTrend";
    public static final String MAX_TREND_VALUE = "maxTrendValue";
    public static final String TICK_SPACING = "tickSpacing";
    public static final String DEFAULT_MODES_COLOR = "defaultModesColor";

}
