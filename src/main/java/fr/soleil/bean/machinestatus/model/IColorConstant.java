package fr.soleil.bean.machinestatus.model;

import java.awt.Color;

public interface IColorConstant {
    public static final Color STANDARD_GRAY_BACKGROUND = new Color(200, 200, 200);

}
