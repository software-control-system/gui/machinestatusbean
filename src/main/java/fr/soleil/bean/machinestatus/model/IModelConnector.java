package fr.soleil.bean.machinestatus.model;

public interface IModelConnector {
    void setDeviceName(String s);

    void connectTargets();

    void disconnectTargets();
}
