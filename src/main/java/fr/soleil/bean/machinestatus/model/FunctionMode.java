package fr.soleil.bean.machinestatus.model;

import java.awt.Color;

// Function mode colors and meanings as defined in CTRLRFC-503
public enum FunctionMode implements IModeConstants {

    // - green
    USER_MODE(USER_MODE_NAME, "(User beam with no top up injection)", new Color(120, 255, 120)),
    // - same color as USER mode, because both modes mean "accessible to beamlines"
    TOP_UP_MODE(TOP_UP_MODE_NAME, "(User beam with top up injection)", new Color(120, 255, 120)),
    // - gray
    STANDBY_MODE(STANDBY_MODE_NAME, "(User beam unavailable, machine setup for beamlines)", new Color(102, 102, 102)),
    // - yellow
    MACHINE_MODE(MACHINE_MODE_NAME, "(Machine dedicated time)", Color.YELLOW),
    // - white
    SHUTDOWN_MODE(SHUTDOWN_MODE_NAME, "(Shutdown period \u2014 no beam)", Color.WHITE),
    // - light blue
    USER_RP_MODE(USER_RP_MODE_NAME, "(Radiation safety test)", Color.CYAN),
    // - gray (darker)
    UNKNOWN_MODE(UNKNOWN_MODE_NAME, "(Unknown function mode)", Color.gray);

    private final String name;
    private final String description;
    private final Color color;

    private FunctionMode(String name, String description, Color color) {
        this.name = name;
        this.description = description;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return getName();
    }
}
