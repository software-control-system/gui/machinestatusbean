package fr.soleil.bean.machinestatus.model;

public interface IModeConstants {

    public static final String USER_MODE_NAME = "USER";
    public static final String TOP_UP_MODE_NAME = "TOP-UP";
    public static final String STANDBY_MODE_NAME = "STANDBY";
    public static final String MACHINE_MODE_NAME = "MACHINE";
    public static final String SHUTDOWN_MODE_NAME = "SHUTDOWN";
    public static final String USER_RP_MODE_NAME = "USER-RP";
    public static final String UNKNOWN_MODE_NAME = "UNKNOWN";

}
