package fr.soleil.bean.machinestatus.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import fr.soleil.bean.machinestatus.model.FrontEndState;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.ComponentMouseDelegate;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.data.target.scalar.ITextComponent;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.text.DynamicForegroundLabel;

public class BeamLineLabel extends DynamicForegroundLabel implements ITextComponent {

    private static final long serialVersionUID = 3881242911828519530L;

    private final ErrorNotificationDelegate errorNotificationDelegate;

    public BeamLineLabel(Font font, String beamName) {
        super();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        setAntiAliasingEnabled(true);
        setFont(font);
        forceText(beamName);
        setOpaque(true);
        // CONTROLGUI-369: BeamLineLabel initialized with Invalid state color
        setBackground(FrontEndState.INVALID.getColor());
        setBorder(BorderFactory.createLineBorder(Color.GRAY));
        setHorizontalAlignment(SwingConstants.CENTER);
        setPreferredSize(null);
    }

    public void forceText(String text) {
        super.setText(text == null ? ObjectUtils.EMPTY_STRING : text);
    }

    @Override
    public void setText(String text) {
        // just for non imposed text
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        setBackground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(getBackground());
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(getForeground());
    }

    @Override
    public void setCometeFont(CometeFont font) {
        setFont(FontTool.getFont(font));
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(getFont());
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
