package fr.soleil.bean.machinestatus.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.machinestatus.MachineStatusBean;
import fr.soleil.bean.machinestatus.model.FunctionMode;
import fr.soleil.bean.machinestatus.model.IMachineStatusConstants;
import fr.soleil.bean.machinestatus.model.IModeConstants;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.comete.swing.chart.util.PlotPropertiesTool;
import fr.soleil.lib.project.ObjectUtils;

public class MachineStatusChart extends Chart implements IModeConstants {

    private static final long serialVersionUID = 7337216243848661721L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MachineStatusBean.class);

    // PROBLEM-1898 and CONTROLGUI-360: compute best font size
    protected static final int DEFAULT_LABEL_FONT_SIZE;
    static {
        if ("windows".equalsIgnoreCase(System.getProperty("os.name", ObjectUtils.EMPTY_STRING))) {
            DEFAULT_LABEL_FONT_SIZE = 15;
        } else {
            String java = System.getProperty("java.version", ObjectUtils.EMPTY_STRING).toLowerCase() + " from "
                    + System.getProperty("java.vendor", ObjectUtils.EMPTY_STRING).toLowerCase() + " installed in "
                    + System.getProperty("java.home", ObjectUtils.EMPTY_STRING).toLowerCase();
            if (java.indexOf("openjdk") < 0) {
                DEFAULT_LABEL_FONT_SIZE = 15;
            } else {
                DEFAULT_LABEL_FONT_SIZE = 17;
            }
        }
    }
    protected static final boolean USE_OLD_METHODS = "true".equalsIgnoreCase(System.getProperty("useOldMethods"));

    private static final Map<String, Color> CHART_COLORS;
    static {
        Map<String, Color> tmpMap = new HashMap<>();
        for (FunctionMode mode : FunctionMode.values()) {
            tmpMap.put(mode.getName(), mode.getColor());
        }
        CHART_COLORS = Collections.unmodifiableMap(tmpMap);
    }
    private static final CometeFont LABEL_FONT = new CometeFont(Font.SANS_SERIF, Font.BOLD, DEFAULT_LABEL_FONT_SIZE);

    private final Map<String, Color> attribColors;
    private String keyInfo;

    private double maxTrendValue, tickSpacing;
    private AxisProperties y1Properties;
    private PlotProperties userMode, topUpMode, standbyMode, machineMode, shutdownMode, userRPMode;
    private String lastModeName;

    public MachineStatusChart() {
        super();
        attribColors = new HashMap<>();
        lastModeName = null;
        keyInfo = IMachineStatusConstants.CURRENT;
        initChartConfiguration(false);
    }

    protected PlotProperties generateAndApplyDataViewProperties(String keyInfo) {
        PlotProperties properties;
        if (keyInfo == null) {
            properties = null;
        } else {
            properties = new PlotProperties(CometeColor.RED);
            properties.setAxisChoice(Y1);
            properties.getCurve().setWidth(1);
            properties.getCurve().setName(keyInfo);
            setDataViewPlotProperties(keyInfo, properties);
        }
        return properties;
    }

    protected PlotProperties generateAndApplyModeProperties(FunctionMode mode) {
        PlotProperties properties = new PlotProperties(CometeColor.RED);
        properties.setAxisChoice(Y1);
        properties.getMarker().setLegendVisible(false);
        properties.getCurve().setName(mode.getName());
        properties.getCurve().setWidth(1);
        properties.getBar().setFillColor(ColorTool.getCometeColor(CHART_COLORS.get(mode.getName())));
        properties.getBar().setFillStyle(IChartViewer.FILL_STYLE_SOLID);
        setDataViewPlotProperties(mode.getName(), properties);
        return properties;
    }

    public void setKeyInfo(String keyInfo) {
        if (!ObjectUtils.sameObject(this.keyInfo, keyInfo)) {
            setDataViewPlotProperties(this.keyInfo, null);
            removeSingleView(this.keyInfo);
            this.keyInfo = keyInfo;
        }
    }

    public void initChartConfiguration(boolean isSampled) {
        // Chart Properties
        resetAll(true);
        lastModeName = null;
        setPreferredSize(new Dimension(1200, 300));
        setMinimumSize(getPreferredSize());
        setGridVisible(true, IChartViewer.Y1);
        setGridVisible(true, IChartViewer.X);
        setManagementPanelVisible(false);
        setFreezePanelVisible(false);
        setLegendVisible(false);
        setRefreshLater(true);
        SamplingProperties properties = getSamplingProperties();
        properties.setEnabled(isSampled);
        setSamplingProperties(properties);

        setLabelCometeFont(LABEL_FONT);
        setCometeBackground(new CometeColor(200, 200, 200));

        // Axis Properties
        y1Properties = getAxisProperties(Y1);
        y1Properties.setLabelFont(LABEL_FONT);
        y1Properties.setAutoScale(false);
        y1Properties.setScaleMax(maxTrendValue);
        y1Properties.setScaleMin(0);
        y1Properties.setUserLabelInterval(tickSpacing);
        setAxisProperties(y1Properties, Y1);

        AxisProperties xProperties = getAxisProperties(X);
        xProperties.setLabelFont(LABEL_FONT);
        setAxisProperties(xProperties, X);

        AxisProperties y2Properties = getAxisProperties(Y2);
        y2Properties.setLabelFont(LABEL_FONT);
        setAxisProperties(y2Properties, Y2);

        // Plot Properties
        generateAndApplyDataViewProperties(keyInfo);
        userMode = generateAndApplyModeProperties(FunctionMode.USER_MODE);
        topUpMode = generateAndApplyModeProperties(FunctionMode.TOP_UP_MODE);
        standbyMode = generateAndApplyModeProperties(FunctionMode.STANDBY_MODE);
        machineMode = generateAndApplyModeProperties(FunctionMode.MACHINE_MODE);
        shutdownMode = generateAndApplyModeProperties(FunctionMode.SHUTDOWN_MODE);
        userRPMode = generateAndApplyModeProperties(FunctionMode.USER_RP_MODE);

    }

    protected void configureProperties(final double maxTrend, final double tick, final Map<String, Color> colors) {
        // if some change in device colors, redo chart config !
        if ((colors != null) && (!colors.isEmpty()) && (!ObjectUtils.sameMapContent(colors, attribColors))) {
            attribColors.putAll(colors);
            userMode.getBar()
                    .setFillColor(ColorTool.getCometeColor(attribColors.get(FunctionMode.USER_MODE.getName())));
            setDataViewPlotProperties(FunctionMode.USER_MODE.getName(), userMode);
            topUpMode.getBar()
                    .setFillColor(ColorTool.getCometeColor(attribColors.get(FunctionMode.TOP_UP_MODE.getName())));
            setDataViewPlotProperties(FunctionMode.TOP_UP_MODE.getName(), topUpMode);
            standbyMode.getBar()
                    .setFillColor(ColorTool.getCometeColor(attribColors.get(FunctionMode.STANDBY_MODE.getName())));
            setDataViewPlotProperties(FunctionMode.STANDBY_MODE.getName(), standbyMode);
            machineMode.getBar()
                    .setFillColor(ColorTool.getCometeColor(attribColors.get(FunctionMode.MACHINE_MODE.getName())));
            setDataViewPlotProperties(FunctionMode.MACHINE_MODE.getName(), machineMode);
            shutdownMode.getBar()
                    .setFillColor(ColorTool.getCometeColor(attribColors.get(FunctionMode.SHUTDOWN_MODE.getName())));
            setDataViewPlotProperties(FunctionMode.SHUTDOWN_MODE.getName(), shutdownMode);
            userRPMode.getBar()
                    .setFillColor(ColorTool.getCometeColor(attribColors.get(FunctionMode.USER_RP_MODE.getName())));
            setDataViewPlotProperties(FunctionMode.USER_RP_MODE.getName(), userRPMode);
            LOGGER.debug("*******    AttributeModesColor applied !!!    ********");
        }
        // update axis properties
        if ((maxTrend != maxTrendValue) || (tick != tickSpacing)) {
            maxTrendValue = maxTrend;
            tickSpacing = tick;
            y1Properties.setScaleMax(maxTrendValue);
            y1Properties.setUserLabelInterval(tickSpacing);
            setAxisProperties(y1Properties, Y1);
        }
    }

    protected Map<String, Object> buildData(final String[] modes, final double[] yValues, final long[] times) {
        Map<String, Object> data = new LinkedHashMap<String, Object>();
        if ((yValues != null) && (times != null) && (keyInfo != null)) {
            // write y values (red curve)
            data.put(keyInfo, createChartData(times, yValues));
            if (modes != null) {
                // update fill colors
                int length = Math.min(yValues.length, modes.length);
                double[] lastMode = null;
                double[] userMode = new double[length];
                double[] topUpMode = new double[length];
                double[] standbyMode = new double[length];
                double[] machineMode = new double[length];
                double[] shutdownMode = new double[length];
                double[] userRPMode = new double[length];
                int index = 0;
                for (String mode : modes) {
                    if (index >= length) {
                        break;
                    }
                    if (FunctionMode.UNKNOWN_MODE.getName().equalsIgnoreCase(mode)) {
                        if (lastModeName == null) {
                            LOGGER.error("beginning chart with UNKNOWN");
                        } else {
                            mode = lastModeName;
                            // LOGGER.debug("We are in UNKNOWN MODE !!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        }
                    }
                    if (FunctionMode.USER_MODE.getName().equalsIgnoreCase(mode)) {
                        userMode[index] = yValues[index];
                        topUpMode[index] = Double.NaN;
                        standbyMode[index] = Double.NaN;
                        machineMode[index] = Double.NaN;
                        shutdownMode[index] = Double.NaN;
                        userRPMode[index] = Double.NaN;
                        // Fill last mode to ensure color continuity
                        if (lastMode != null) {
                            lastMode[index] = yValues[index];
                        }
                        lastMode = userMode;
                        lastModeName = FunctionMode.USER_MODE.getName();
                    } else if (FunctionMode.TOP_UP_MODE.getName().equalsIgnoreCase(mode)) {
                        userMode[index] = Double.NaN;
                        topUpMode[index] = yValues[index];
                        standbyMode[index] = Double.NaN;
                        machineMode[index] = Double.NaN;
                        shutdownMode[index] = Double.NaN;
                        userRPMode[index] = Double.NaN;
                        // Fill last mode to ensure color continuity
                        if (lastMode != null) {
                            lastMode[index] = yValues[index];
                        }
                        lastMode = topUpMode;
                        lastModeName = FunctionMode.TOP_UP_MODE.getName();
                    } else if (FunctionMode.STANDBY_MODE.getName().equalsIgnoreCase(mode)) {
                        userMode[index] = Double.NaN;
                        topUpMode[index] = Double.NaN;
                        standbyMode[index] = yValues[index];
                        machineMode[index] = Double.NaN;
                        shutdownMode[index] = Double.NaN;
                        userRPMode[index] = Double.NaN;
                        // Fill last mode to ensure color continuity
                        if (lastMode != null) {
                            lastMode[index] = yValues[index];
                        }
                        lastMode = standbyMode;
                        lastModeName = FunctionMode.STANDBY_MODE.getName();
                    } else if (FunctionMode.MACHINE_MODE.getName().equalsIgnoreCase(mode)) {
                        userMode[index] = Double.NaN;
                        topUpMode[index] = Double.NaN;
                        standbyMode[index] = Double.NaN;
                        machineMode[index] = yValues[index];
                        shutdownMode[index] = Double.NaN;
                        userRPMode[index] = Double.NaN;
                        // Fill last mode to ensure color continuity
                        if (lastMode != null) {
                            lastMode[index] = yValues[index];
                        }
                        lastMode = machineMode;
                        lastModeName = FunctionMode.MACHINE_MODE.getName();
                    } else if (FunctionMode.SHUTDOWN_MODE.getName().equalsIgnoreCase(mode)) {
                        userMode[index] = Double.NaN;
                        topUpMode[index] = Double.NaN;
                        standbyMode[index] = Double.NaN;
                        machineMode[index] = Double.NaN;
                        shutdownMode[index] = yValues[index];
                        userRPMode[index] = Double.NaN;
                        // Fill last mode to ensure color continuity
                        if (lastMode != null) {
                            lastMode[index] = yValues[index];
                        }
                        lastMode = shutdownMode;
                        lastModeName = FunctionMode.SHUTDOWN_MODE.getName();
                    } else if (FunctionMode.USER_RP_MODE.getName().equalsIgnoreCase(mode)) {
                        userMode[index] = Double.NaN;
                        topUpMode[index] = Double.NaN;
                        standbyMode[index] = Double.NaN;
                        machineMode[index] = Double.NaN;
                        shutdownMode[index] = Double.NaN;
                        userRPMode[index] = yValues[index];
                        // Fill last mode to ensure color continuity
                        if (lastMode != null) {
                            lastMode[index] = yValues[index];
                        }
                        lastMode = userRPMode;
                        lastModeName = FunctionMode.USER_RP_MODE.getName();
                    } /*else {
                        userMode[index] = Double.NaN;
                        topUpMode[index] = Double.NaN;
                        standbyMode[index] = Double.NaN;
                        machineMode[index] = Double.NaN;
                        shutdownMode[index] = Double.NaN;
                        userRPMode[index] = Double.NaN;
                        lastMode = null;
                      }*/
                    index++;
                }
                data.put(FunctionMode.USER_MODE.getName(), createChartData(times, userMode));
                data.put(FunctionMode.TOP_UP_MODE.getName(), createChartData(times, topUpMode));
                data.put(FunctionMode.STANDBY_MODE.getName(), createChartData(times, standbyMode));
                data.put(FunctionMode.MACHINE_MODE.getName(), createChartData(times, machineMode));
                data.put(FunctionMode.SHUTDOWN_MODE.getName(), createChartData(times, shutdownMode));
                data.put(FunctionMode.USER_RP_MODE.getName(), createChartData(times, userRPMode));
            }
            LOGGER.debug("Updated data number = {}", data.size());
        } else {
            LOGGER.error("keyInfo is empty update chart cannot be done");
        }
        return data;
    }

    protected DataView getPreparedDataView(String id, boolean clickable) {
        DataView view = dataViewList.getDataView(id, false);
        if (view == null) {
            view = new DataView(id);
            view.setXDataSorted(isDataViewsSortedOnX());
            view.setAxis(Y1);
            PlotPropertiesTool.writePlotProperties(view, getPlotPropertiesForNewDataView(id, view.getDisplayName()));
            view.setClickable(clickable);
            view.setLabelVisible(clickable);
            view = dataViewList.addDataViewIfAbsent(view);
        }
        view.reset();
        return view;
    }

    protected void checkDataViewFilters(DataView view, boolean viewChanged) {
        if (viewChanged) {
            view.updateFilters();
        }
        updateExpressions(view);
    }

    // PROBLEM-1784: In order to consume less memory:
    // - Don't create sub data arrays and directly update DataViews.
    // - For every DataView, use 1 and only 1 NaN point per gap.
    protected void updateDataViews(final String[] modes, final double[] yValues, final long[] times) {
        if ((yValues != null) && (times != null) && (keyInfo != null)) {
            DataView yView = getPreparedDataView(keyInfo, true);
            DataView userView = getPreparedDataView(FunctionMode.USER_MODE.getName(), false);
            DataView topUpView = getPreparedDataView(FunctionMode.TOP_UP_MODE.getName(), false);
            DataView standbyView = getPreparedDataView(FunctionMode.STANDBY_MODE.getName(), false);
            DataView machineView = getPreparedDataView(FunctionMode.MACHINE_MODE.getName(), false);
            DataView shutdownView = getPreparedDataView(FunctionMode.SHUTDOWN_MODE.getName(), false);
            DataView userRpView = getPreparedDataView(FunctionMode.USER_RP_MODE.getName(), false);
            boolean yViewChanged = false;
            boolean userViewChanged = false;
            boolean topUpViewChanged = false;
            boolean standbyViewChanged = false;
            boolean machineViewChanged = false;
            boolean shutdownViewChanged = false;
            boolean userRpViewChanged = false;
            int length = Math.min(yValues.length, times.length);
            if (modes == null) {
                for (int i = 0; i < length; i++) {
                    yView.add(times[i], yValues[i], false);
                    yViewChanged = true;
                }
            } else {
                length = Math.min(length, modes.length);
                boolean nanUser = false, nantopUp = false, nanStandby = false, nanMachine = false, nanShutdown = false,
                        nanUserRp = false;
                for (int i = 0; i < length; i++) {
                    yView.add(times[i], yValues[i]);
                    yViewChanged = true;
                    String mode = modes[i];
                    if (mode == null) {
                        mode = FunctionMode.UNKNOWN_MODE.getName();
                    } else if (FunctionMode.UNKNOWN_MODE.getName().equalsIgnoreCase(mode)) {
                        if (lastModeName == null) {
                            LOGGER.error("beginning chart with UNKNOWN");
                        } else {
                            mode = lastModeName;
                            // LOGGER.debug("We are in UNKNOWN MODE !!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        }
                    } else {
                        mode = mode.toUpperCase();
                    }
                    switch (mode) {
                        case USER_MODE_NAME:
                            userView.add(times[i], yValues[i], false);
                            userViewChanged = true;
                            nanUser = false;
                            lastModeName = FunctionMode.USER_MODE.getName();
                            // Add 1 NaN value to other DataViews only if their last point is not already NaN
                            if (!nantopUp) {
                                nantopUp = true;
                                topUpView.add(times[i], Double.NaN);
                            }
                            if (!nanStandby) {
                                nanStandby = true;
                                standbyView.add(times[i], Double.NaN);
                            }
                            if (!nanMachine) {
                                nanMachine = true;
                                machineView.add(times[i], Double.NaN);
                            }
                            if (!nanShutdown) {
                                nanShutdown = true;
                                shutdownView.add(times[i], Double.NaN);
                            }
                            if (!nanUserRp) {
                                nanUserRp = true;
                                userRpView.add(times[i], Double.NaN);
                            }
                            break;
                        case TOP_UP_MODE_NAME:
                            topUpView.add(times[i], yValues[i], false);
                            topUpViewChanged = true;
                            nantopUp = false;
                            lastModeName = FunctionMode.TOP_UP_MODE.getName();
                            // Add 1 NaN value to other DataViews only if their last point is not already NaN
                            if (!nanUser) {
                                nanUser = true;
                                userView.add(times[i], Double.NaN);
                            }
                            if (!nanStandby) {
                                nanStandby = true;
                                standbyView.add(times[i], Double.NaN);
                            }
                            if (!nanMachine) {
                                nanMachine = true;
                                machineView.add(times[i], Double.NaN);
                            }
                            if (!nanShutdown) {
                                nanShutdown = true;
                                shutdownView.add(times[i], Double.NaN);
                            }
                            if (!nanUserRp) {
                                nanUserRp = true;
                                userRpView.add(times[i], Double.NaN);
                            }
                            break;
                        case STANDBY_MODE_NAME:
                            standbyView.add(times[i], yValues[i], false);
                            standbyViewChanged = true;
                            nanStandby = false;
                            lastModeName = FunctionMode.STANDBY_MODE.getName();
                            // Add 1 NaN value to other DataViews only if their last point is not already NaN
                            if (!nanUser) {
                                nanUser = true;
                                userView.add(times[i], Double.NaN);
                            }
                            if (!nantopUp) {
                                nantopUp = true;
                                topUpView.add(times[i], Double.NaN);
                            }
                            if (!nanMachine) {
                                nanMachine = true;
                                machineView.add(times[i], Double.NaN);
                            }
                            if (!nanShutdown) {
                                nanShutdown = true;
                                shutdownView.add(times[i], Double.NaN);
                            }
                            if (!nanUserRp) {
                                nanUserRp = true;
                                userRpView.add(times[i], Double.NaN);
                            }
                            break;
                        case MACHINE_MODE_NAME:
                            machineView.add(times[i], yValues[i], false);
                            machineViewChanged = true;
                            nanMachine = false;
                            lastModeName = FunctionMode.MACHINE_MODE.getName();
                            // Add 1 NaN value to other DataViews only if their last point is not already NaN
                            if (!nanUser) {
                                nanUser = true;
                                userView.add(times[i], Double.NaN);
                            }
                            if (!nantopUp) {
                                nantopUp = true;
                                topUpView.add(times[i], Double.NaN);
                            }
                            if (!nanStandby) {
                                nanStandby = true;
                                standbyView.add(times[i], Double.NaN);
                            }
                            if (!nanShutdown) {
                                nanShutdown = true;
                                shutdownView.add(times[i], Double.NaN);
                            }
                            if (!nanUserRp) {
                                nanUserRp = true;
                                userRpView.add(times[i], Double.NaN);
                            }
                            break;
                        case SHUTDOWN_MODE_NAME:
                            shutdownView.add(times[i], yValues[i], false);
                            shutdownViewChanged = true;
                            nanShutdown = false;
                            lastModeName = FunctionMode.SHUTDOWN_MODE.getName();
                            // Add 1 NaN value to other DataViews only if their last point is not already NaN
                            if (!nanUser) {
                                nanUser = true;
                                userView.add(times[i], Double.NaN);
                            }
                            if (!nantopUp) {
                                nantopUp = true;
                                topUpView.add(times[i], Double.NaN);
                            }
                            if (!nanStandby) {
                                nanStandby = true;
                                standbyView.add(times[i], Double.NaN);
                            }
                            if (!nanMachine) {
                                nanMachine = true;
                                machineView.add(times[i], Double.NaN);
                            }
                            if (!nanUserRp) {
                                nanUserRp = true;
                                userRpView.add(times[i], Double.NaN);
                            }
                            break;
                        case USER_RP_MODE_NAME:
                            userRpView.add(times[i], yValues[i], false);
                            userRpViewChanged = true;
                            nanUserRp = false;
                            lastModeName = FunctionMode.USER_RP_MODE.getName();
                            // Add 1 NaN value to other DataViews only if their last point is not already NaN
                            if (!nanUser) {
                                nanUser = true;
                                userView.add(times[i], Double.NaN);
                            }
                            if (!nantopUp) {
                                nantopUp = true;
                                topUpView.add(times[i], Double.NaN);
                            }
                            if (!nanStandby) {
                                nanStandby = true;
                                standbyView.add(times[i], Double.NaN);
                            }
                            if (!nanMachine) {
                                nanMachine = true;
                                machineView.add(times[i], Double.NaN);
                            }
                            if (!nanShutdown) {
                                nanShutdown = true;
                                shutdownView.add(times[i], Double.NaN);
                            }
                            break;
                        default:
                            // Add 1 NaN value to all DataViews only if their last point is not already NaN
                            if (!nanUser) {
                                nanUser = true;
                                userView.add(times[i], Double.NaN);
                            }
                            if (!nantopUp) {
                                nantopUp = true;
                                topUpView.add(times[i], Double.NaN);
                            }
                            if (!nanStandby) {
                                nanStandby = true;
                                standbyView.add(times[i], Double.NaN);
                            }
                            if (!nanMachine) {
                                nanMachine = true;
                                machineView.add(times[i], Double.NaN);
                            }
                            if (!nanShutdown) {
                                nanShutdown = true;
                                shutdownView.add(times[i], Double.NaN);
                            }
                            if (!nanUserRp) {
                                nanUserRp = true;
                                userRpView.add(times[i], Double.NaN);
                            }
                            break;
                    }
                } // end for (int i = 0; i < length; i++)
            } // end if (modes == null) ... else
            checkDataViewFilters(yView, yViewChanged);
            checkDataViewFilters(userView, userViewChanged);
            checkDataViewFilters(topUpView, topUpViewChanged);
            checkDataViewFilters(standbyView, standbyViewChanged);
            checkDataViewFilters(machineView, machineViewChanged);
            checkDataViewFilters(shutdownView, shutdownViewChanged);
            checkDataViewFilters(userRpView, userRpViewChanged);
        } // end if ((yValues != null) && (times != null) && (keyInfo != null))
    }

    // PROBLEM-1784: In order to consume less memory: Once dataviews are up to date, refresh chart view in EDT
    protected void refreshDisplayAfterDataChange() {
        refreshVisibilityBoxes(true, null);
        updateFreezePanelAvailability();
        Window dataWindow = dataTableWindow;
        if ((dataWindow != null) && (dataWindow.isVisible())) {
            updateTableContent();
        }
    }

    public void updateChartData(final String[] modes, final double[] yValues, final long[] times, final double maxTrend,
            final double tick, final Map<String, Color> colors) {

        SwingWorker<Map<String, Object>, Void> worker = new SwingWorker<Map<String, Object>, Void>() {
            @Override
            protected Map<String, Object> doInBackground() throws Exception {
                configureProperties(maxTrend, tick, colors);
                Map<String, Object> data;
                if (USE_OLD_METHODS) {
                    // old method: create double arrays and call setData
                    data = buildData(modes, yValues, times);
                } else {
                    // new method: create and update DataViews
                    data = null;
                    updateDataViews(modes, yValues, times);
                }
                return data;
            }

            @Override
            protected void done() {
                try {
                    Map<String, Object> data = get();
                    if (USE_OLD_METHODS) {
                        // old method: create double arrays and call setData
                        setData(data);
                    } else {
                        // new method: create and update DataViews
                        refreshDisplayAfterDataChange();
                    }
                    if ((times != null) && (times.length > 0)) {
                        setDisplayDuration(times.length * 1000);
                    }
                } catch (Exception e) {
                    LOGGER.error("Error setting data to the chart : ", e);
                }
            }
        };
        worker.execute();
    }

    // creates some data a chart can understand
    protected Object createChartData(Object xData, Object yData) {
        Object[] result;
        if ((xData == null) || (yData == null)) {
            result = new Object[0];
        } else {
            result = new Object[2];
            result[IChartViewer.Y_INDEX] = yData;
            result[IChartViewer.X_INDEX] = xData;
        }
        return result;
    }

    public void clearChart() {
        setKeyInfo(null);
        resetAll();
        setData(null);
        LOGGER.debug("chart cleared !!!!");
    }

}