package fr.soleil.bean.machinestatus.customtargets;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.matrix.INumberMatrixTarget;

public abstract class NumberMatrixTargetAdapter implements INumberMatrixTarget {

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return 0;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return 0;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
    }

    @Override
    public Object[] getNumberMatrix() {
        return null;
    }

    @Override
    public Object getFlatNumberMatrix() {
        return null;
    }

}
