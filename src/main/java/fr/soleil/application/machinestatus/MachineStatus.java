/**
 *
 */
package fr.soleil.application.machinestatus;

import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import java.util.Arrays;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.machinestatus.MachineStatusBean;
import fr.soleil.lib.project.application.Application;
import fr.soleil.lib.project.resource.MessageManager;

public class MachineStatus extends Application {

    private static final long serialVersionUID = 2545244771595575025L;

    private static final String TANGO_HOST = "TANGO_HOST";
    private static final String COPYRIGHT = "SOLEIL Synchrotron";
    private static final String TITLE = "Machine Status";
    private static final ImageIcon SPASH_BG = new ImageIcon(
            MachineStatusBean.class.getResource("/fr/soleil/bean/machinestatus/image/splash-11986-2.png"));
    private static final ImageIcon SMALL_ICON = new ImageIcon(
            MachineStatusBean.class.getResource("/fr/soleil/bean/machinestatus/image/logo-ZEsoleil-25x23.png"));

    private static final Logger LOGGER = LoggerFactory.getLogger(MachineStatus.class);

    private static void createAndShowGui(String[] args) {
        if (args.length == 1) {
            final String deviceName = args[0];
            MessageManager messageManager = getMessageManager(MachineStatus.class.getClassLoader(),
                    MachineStatus.class.getPackage().getName());

            final MachineStatus ms = new MachineStatus();
            ms.setDefaultCloseOperation(EXIT_ON_CLOSE);
            ms.getSplash().setMaxProgress(5);
            ms.getSplash().progress(0);
            ms.setSize(1280, 1024);
            ms.setJMenuBar(null);
            final ResourceBundle rb = ResourceBundle
                    .getBundle(MachineStatus.class.getPackage().getName() + ".application");
            final String version = rb.getString("project.name") + " " + rb.getString("project.version");
            ms.setTitle(version);
            ms.setLocationRelativeTo(null);
            ms.setDefaultCloseOperation(EXIT_ON_CLOSE);
            ms.getSplash().progress(2);
            ms.getSplash().setMessage(messageManager.getMessage("Application.progressMessage2"));
            final MachineStatusBean bean = new MachineStatusBean() {
                private static final long serialVersionUID = 6889726960536878604L;

                @Override
                public void refreshGUI() {
                    super.refreshGUI();
                    ms.getSplash().progress(5);
                    ms.getSplash().setVisible(false);
                }
            };
            ms.getSplash().progress(3);
            ms.getSplash().setMessage(messageManager.getMessage("Application.progressMessage3"));
            ms.getSplash().progress(4);
            ms.getSplash().setMessage(messageManager.getMessage("Application.progressMessage4"));
            ms.setContentPane(bean);
            ms.setVisible(true);
            ms.getSplash().setVisible(true);
            SwingUtilities.invokeLater(() -> {
                bean.setModel(deviceName);
                bean.start();
                ms.toFront();
            });

        } else {
            System.out.println("Usage : MachineStatus \"deviceName\" ");
        }
    }

    @Override
    protected void doQuit() {
        Container content = getContentPane();
        if (content instanceof MachineStatusBean) {
            ((MachineStatusBean) content).stop();
        }
        super.doQuit();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void setVisible(boolean visible) {
        // stupid, but used to avoid undesired splash update
        show(visible);
    }

    public static void main(final String[] args) {

        LOGGER.debug("starting machine status with args: {}", Arrays.toString(args));

        // Check Tango_HOST
        if (System.getProperty(TANGO_HOST) == null) {
            String tangoHost = System.getenv(TANGO_HOST);
            if ((tangoHost == null) || (tangoHost.trim().isEmpty())) {
                tangoHost = "dev-db1:20001,dev-db1:20002";
            }

            System.getProperties().put(TANGO_HOST, tangoHost);
        }

        SwingUtilities.invokeLater(() -> {
            createAndShowGui(args);
        });
    }

    @Override
    protected Color getSplashForeground() {
        return Color.BLACK;
    }

    @Override
    protected ImageIcon getSplashImage() {
        return SPASH_BG;
    }

    @Override
    protected Image getDefaultIconImage() {
        return SMALL_ICON.getImage();
    }

    @Override
    protected String getApplicationTitle() {
        return TITLE;
    }

    @Override
    protected String getCopyright() {
        return COPYRIGHT;
    }

}
